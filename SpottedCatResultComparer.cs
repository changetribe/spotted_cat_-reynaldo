﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpottedCatChallenge
{
    public class SpottedCatResultComparer : IComparer<SpottedCatChallengeResult>
    {
        public int Compare(SpottedCatChallengeResult x, SpottedCatChallengeResult y)
        {
            int returnVal = -1;
            Double probx = x.ImageIsSpottedCatConfidenceLevel;
            Double proby = y.ImageIsSpottedCatConfidenceLevel;

            if (probx == proby)
            {
                returnVal = 0;
            }
            else
            {
                returnVal = (probx < proby) ? -1 : 1;
            }

            return returnVal;
        }
 
    }
}
