﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emgu.CV;
using Emgu.CV.VideoSurveillance;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;

namespace SpottedCatChallenge
{
    /// <summary>
    /// Very simple yet useful class to Substract background (made ad-hoc for this challenge, could possible improve with more time)
    /// </summary>
    public class ImageBackgroundSubstractor
    {
        private Image<Bgr, byte> img1Masked;
        private Image<Bgr, byte> img2Masked;

        public Image<Bgr, byte> Img1Masked
        {
            get { return img1Masked; }
        }

        public Image<Bgr, byte> Img2Masked
        {
            get { return img2Masked; }
        }

        public Image<Bgr, byte> SubstractBackground(Image<Bgr, byte> img1, Image<Bgr, byte> img2)
        {
            BackgroundSubtractorMOG2 bckgSubs = new BackgroundSubtractorMOG2(0, 50, false);

            //  Image<Bgr, Byte> img1 = new Image<Bgr, Byte>(@"C:\SpottedCat\Images\TEST\105426507922298233889447262088403777181_0.jpg");
            //  Image<Bgr, Byte> img2 = new Image<Bgr, Byte>(@"C:\SpottedCat\Images\TEST\105426507922298233889447262088403777181_1.jpg");

            //Image<Bgr, Byte> img1 = new Image<Bgr, Byte>(@"C:\SpottedCat\Images\TEST\100444873256622600673207507139533961750_0.jpg");
            //Image<Bgr, Byte> img1 = new Image<Bgr, Byte>(@"C:\SpottedCat\Images\TEST\100444873256622600673207507139533961750_1.jpg");
            //Image<Bgr, Byte> img2 = new Image<Bgr, Byte>(@"C:\SpottedCat\Images\TEST\100444873256622600673207507139533961750_2.jpg");

            //Image<Bgr, Byte> img1 = new Image<Bgr, Byte>(@"C:\SpottedCat\Images\TEST\100564113623119557673065188467566073579_1.jpg");
            //Image<Bgr, Byte> img2 = new Image<Bgr, Byte>(@"C:\SpottedCat\Images\TEST\100564113623119557673065188467566073579_2.jpg");


            bckgSubs.Update(img1.SmoothGaussian(11));
            bckgSubs.Update(img2.SmoothGaussian(11));

            Image<Gray, Byte> foreMask = bckgSubs.ForegroundMask;
            //Image<Gray, Byte> back = bckgSubs.BackgroundMask;

            // MORPH OPEN (here I use CLOSE since mask is inverted)
            StructuringElementEx element = new StructuringElementEx(6, 6, 3, 3, CV_ELEMENT_SHAPE.CV_SHAPE_ELLIPSE);
            foreMask = foreMask.MorphologyEx(element, CV_MORPH_OP.CV_MOP_CLOSE, 5);

            Image<Gray, Byte>[] channels = new Image<Gray, byte>[3];
            channels[0] = foreMask;
            channels[1] = foreMask;
            channels[2] = foreMask;
            Image<Bgr, byte> foreMaskBgr = new Image<Bgr, byte>(channels);

            // Calculate foreground in both images
            img1Masked = img1.And(foreMaskBgr);
            img2Masked = img2.And(foreMaskBgr);
            return img1Masked;
        }

    }
}
