﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace SpottedCatChallenge
{
    [Serializable]
    [XmlRoot]
    public class SpottedCatChallengeResult
    {
        private string observedImage;
        private int matchedFeatures;
        private bool imageIsSpottedCat; // if according to the the level of trust set, whether observedImage is a spotted cat or not
        private double imageIsSpottedCatConfidenceLevel; // 'probability' (in the sense of confidence level) of observedImage being actually a spotted cat

        public string ObservedImage
        {
            get { return observedImage; }
            set { observedImage = value; }
        }

        public int MatchedFeatures
        {
            get { return matchedFeatures; }
            set { matchedFeatures = value; }
        }

        public double ImageIsSpottedCatConfidenceLevel
        {
            get { return imageIsSpottedCatConfidenceLevel; }
            set { imageIsSpottedCatConfidenceLevel = value; }
        }

        public bool ImageIsSpottedCat
        {
            get { return imageIsSpottedCat; }
            set { imageIsSpottedCat = value; }
        }

        public SpottedCatChallengeResult(string observedImage, 
            int matchedFeatures, 
            double imageIsSpottedCatConfidenceLevel,
            bool imageIsSpottedCat)
        {
            this.observedImage = observedImage;
            this.matchedFeatures = matchedFeatures;
            this.imageIsSpottedCat = imageIsSpottedCat;
            this.imageIsSpottedCatConfidenceLevel = imageIsSpottedCatConfidenceLevel;
        }

        public SpottedCatChallengeResult() { 
        }
    }
}
