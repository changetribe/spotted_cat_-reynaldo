﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace SpottedCatChallenge.MeasurementHelpers
{
    public class SolutionVerifier
    {
        private List<string> knownSpottedCats;
        private ReceiverOperatingCharacteristic roc;

        public ReceiverOperatingCharacteristic ROC
        {
            get { return roc; }
            set { roc = value; }
        }

        public SolutionVerifier(bool debugMode, string sourcePath) 
        {
            if (debugMode)
            {
                var spottedcats = File.ReadAllLines(Path.Combine(sourcePath, "SpottedCatList.txt"));
                knownSpottedCats = new List<string>(spottedcats);
                knownSpottedCats.Sort();
            }
            else
            {
                knownSpottedCats = new List<string>();
            }

            roc = new ReceiverOperatingCharacteristic();
        }
        
        
        public bool IsSolutionOK(string observedFileName, bool isSpottedCat) 
        {
            int idx = knownSpottedCats.BinarySearch(observedFileName.ToUpper());
            bool isReallySpottedCat = (idx >= 0);

            roc.AddDetection(isReallySpottedCat, isSpottedCat);

            // explanation : we need to return true whenever 
            // isSpottedCat AND isReallySpottedCat
            // OR 
            // !isSpottedCat AND !isReallySpottedCat
            
            return (isSpottedCat == isReallySpottedCat);
        }
    }
}
