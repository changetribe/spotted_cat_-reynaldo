﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpottedCatChallenge.MeasurementHelpers
{
    public class ReceiverOperatingCharacteristic
    {
        int truePositives;
        int falsePositives;
        int trueNegatives;
        int falseNegatives;

        public ReceiverOperatingCharacteristic()
        {
            truePositives = 0;
            falsePositives = 0;
            trueNegatives = 0;
            falseNegatives = 0;
        }

        public int P
        {
            get { return truePositives + falseNegatives; }
        }
        public int N
        {
            get { return falsePositives + trueNegatives; }
        }

        // ------------------------------------------
        public int TruePositives
        {
            get { return truePositives; }
        }
        public int FalsePositives
        {
            get { return falsePositives; }
        }
        public int TrueNegatives
        {
            get { return trueNegatives; }
        }
        public int FalseNegatives
        {
            get { return falseNegatives; }
        }

        public void IncTruePositives() { truePositives++; }
        public void IncTrueNegatives() { trueNegatives++; }
        public void IncFalsePositives() { falsePositives++; }
        public void IncFalseNegatives() { falseNegatives++; }

        public int Total 
        {
            get { return P + N; }
        }
        public double TPR
        {
            get { return (double)truePositives / (double)P; }
        }
        public double FPR
        {
            get { return (double)falsePositives / (double)N; }
        }
        public double FNR
        {
            get { return (double)falseNegatives/ (double)P; }
        }
        public double TNR
        {
            get { return (double)trueNegatives / (double)N; }
        }

        public double ACC
        {
            get { return (double) (truePositives + trueNegatives) / (double) Total;}
        }

        public void AddDetection(bool realResult, bool detectionResult) 
        {
            if (realResult)
            {
                if (detectionResult)
                {
                    IncTruePositives();
                }
                else
                {
                    IncFalseNegatives();
                }
            }
            else {
                if (detectionResult)
                {
                    IncFalsePositives();
                }
                else
                {
                    IncTrueNegatives();
                }
            }
        }


        public void Reset()
        {
            truePositives = 0;
            falsePositives = 0;
            trueNegatives = 0;
            falseNegatives = 0;
        }
    }
}
