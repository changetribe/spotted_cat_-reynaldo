﻿namespace SpottedCatChallenge
{
    partial class FormSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.sensitivityUpDown = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkboxGenDebugImgsPOS = new System.Windows.Forms.CheckBox();
            this.chkboxOverrideFolder = new System.Windows.Forms.CheckBox();
            this.chkboxGenDebugImgsNEG = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sensitivityUpDown)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOK.Location = new System.Drawing.Point(370, 540);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(171, 31);
            this.btnOK.TabIndex = 30;
            this.btnOK.Text = "&OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.CausesValidation = false;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(193, 540);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(171, 31);
            this.btnCancel.TabIndex = 31;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.sensitivityUpDown);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(529, 342);
            this.groupBox1.TabIndex = 32;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Detector Settings";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(178, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "%";
            // 
            // sensitivityUpDown
            // 
            this.sensitivityUpDown.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sensitivityUpDown.Location = new System.Drawing.Point(100, 34);
            this.sensitivityUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.sensitivityUpDown.Name = "sensitivityUpDown";
            this.sensitivityUpDown.Size = new System.Drawing.Size(69, 26);
            this.sensitivityUpDown.TabIndex = 1;
            this.sensitivityUpDown.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Sensitivity";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkboxGenDebugImgsNEG);
            this.groupBox2.Controls.Add(this.chkboxGenDebugImgsPOS);
            this.groupBox2.Controls.Add(this.chkboxOverrideFolder);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(12, 375);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(529, 139);
            this.groupBox2.TabIndex = 33;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Other Settings";
            // 
            // chkboxGenDebugImgsPOS
            // 
            this.chkboxGenDebugImgsPOS.AutoSize = true;
            this.chkboxGenDebugImgsPOS.Checked = true;
            this.chkboxGenDebugImgsPOS.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkboxGenDebugImgsPOS.Location = new System.Drawing.Point(19, 69);
            this.chkboxGenDebugImgsPOS.Name = "chkboxGenDebugImgsPOS";
            this.chkboxGenDebugImgsPOS.Size = new System.Drawing.Size(326, 20);
            this.chkboxGenDebugImgsPOS.TabIndex = 1;
            this.chkboxGenDebugImgsPOS.Tag = "s";
            this.chkboxGenDebugImgsPOS.Text = "&Generate debug images in output folder for positives";
            this.chkboxGenDebugImgsPOS.UseVisualStyleBackColor = true;
            // 
            // chkboxOverrideFolder
            // 
            this.chkboxOverrideFolder.AutoSize = true;
            this.chkboxOverrideFolder.Location = new System.Drawing.Point(19, 34);
            this.chkboxOverrideFolder.Name = "chkboxOverrideFolder";
            this.chkboxOverrideFolder.Size = new System.Drawing.Size(244, 20);
            this.chkboxOverrideFolder.TabIndex = 0;
            this.chkboxOverrideFolder.Tag = "s";
            this.chkboxOverrideFolder.Text = "O&verride output folder if already exists";
            this.chkboxOverrideFolder.UseVisualStyleBackColor = true;
            // 
            // chkboxGenDebugImgsNEG
            // 
            this.chkboxGenDebugImgsNEG.AutoSize = true;
            this.chkboxGenDebugImgsNEG.Checked = true;
            this.chkboxGenDebugImgsNEG.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkboxGenDebugImgsNEG.Location = new System.Drawing.Point(19, 104);
            this.chkboxGenDebugImgsNEG.Name = "chkboxGenDebugImgsNEG";
            this.chkboxGenDebugImgsNEG.Size = new System.Drawing.Size(330, 20);
            this.chkboxGenDebugImgsNEG.TabIndex = 2;
            this.chkboxGenDebugImgsNEG.Tag = "s";
            this.chkboxGenDebugImgsNEG.Text = "&Generate debug images in output folder for negatives";
            this.chkboxGenDebugImgsNEG.UseVisualStyleBackColor = true;
            // 
            // FormSettings
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(553, 583);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FormSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Change detection settings";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sensitivityUpDown)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown sensitivityUpDown;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox chkboxOverrideFolder;
        private System.Windows.Forms.CheckBox chkboxGenDebugImgsPOS;
        private System.Windows.Forms.CheckBox chkboxGenDebugImgsNEG;
    }
}