﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace SpottedCatChallenge
{
    [Serializable]
    public class SpottedCatChallengeInput
    {
        private List<FileInfo> inputFiles;
        private double confidenceLevelZeroToOne;

        public SpottedCatChallengeInput()
        {
            inputFiles = new List<FileInfo>();
            confidenceLevelZeroToOne = 0.8; // default to 80% (fair enough ? :) )
        }

        /// <summary>
        /// Expresses how restrictive detection would be. 
        /// Values near 1 implies probably less detections, but more confidence on true positive results.
        /// On the other hand, values near to zero imply probably a lot of false positives
        /// </summary>
        public double ConfidenceLevelZeroToOne
        {
            get { return confidenceLevelZeroToOne; }
            set { confidenceLevelZeroToOne = value; }
        }

        /// <summary>
        /// Input files paths
        /// </summary>
        public List<FileInfo> InputFiles
        {
            get { return inputFiles; }
            set { inputFiles = value; }
        }
    }
}
