﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

using Emgu.CV.Structure;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using System.Drawing;



// Copyright https://code.google.com/p/parsley/source/browse/branches/lizard/Playground/EllipseDetection.cs?r=330
namespace SpottedCatChallenge
{
    public class EllipseDetection
    {
        public Emgu.CV.Image<Bgr, byte> combine(Emgu.CV.Image<Gray, byte> img) 
        {
            Emgu.CV.Image<Gray, Byte>[] channels = new Emgu.CV.Image<Gray, byte>[3];
            channels[0] = img;
            channels[1] = img;
            channels[2] = img;

            return new Emgu.CV.Image<Bgr, byte>(channels);
        }
        public Emgu.CV.Image<Bgr, byte> ProcessImage(Emgu.CV.Image<Emgu.CV.Structure.Bgr, byte> image)
        {
            Emgu.CV.Image<Bgr, byte> result = image;
            int _min_contour_count = 40;
            double _distance_threshold = 1000.0;
            //Emgu.CV.Image<Gray, byte> gray = image.Convert<Gray, byte>();
            //gray._ThresholdBinary(new Gray(_threshold), new Gray(255.0));
            //gray._Not();

            //Emgu.CV.Contour<System.Drawing.Point> c = gray.FindContours();
             float[,] loGKernelSigma14 = {
                                            {0,0,3,2,2,2,3,0,0},
                                            {0,2,3,5,5,5,3,2,0},
                                            {3,3,5,3,0,3,5,3,3},
                                            {2,5,3,-12,-23,-12,3,5,2},
                                            {2,5,0,-23,-40,-23,0,5,2},
                                            {2,5,3,-12,-23,-12,3,5,2},
                                            {3,3,5,3,0,3,5,3,3},
                                            {0,2,3,5,5,5,3,2,0},
                                            {0,0,3,2,2,2,3,0,0},
                                            };
             Emgu.CV.Image<Gray, Byte> observedImageGray = image.Convert<Gray, Byte>().Not();
             observedImageGray._EqualizeHist();

             
             //observedImageGray = observedImageGray.SmoothGaussian(3);
             //observedImageGray = observedImageGray.Dilate(1);

             int morphOpSize = 89;
             Emgu.CV.StructuringElementEx element = new Emgu.CV.StructuringElementEx(morphOpSize, morphOpSize, morphOpSize / 2, morphOpSize / 2, Emgu.CV.CvEnum.CV_ELEMENT_SHAPE.CV_SHAPE_ELLIPSE);

             Emgu.CV.Image<Gray, byte> filtered = observedImageGray.MorphologyEx(element, Emgu.CV.CvEnum.CV_MORPH_OP.CV_MOP_TOPHAT, 1);
             //filtered = filtered.SmoothGaussian(3);



             filtered = filtered.ThresholdAdaptive(new Gray(255), Emgu.CV.CvEnum.ADAPTIVE_THRESHOLD_TYPE.CV_ADAPTIVE_THRESH_GAUSSIAN_C, Emgu.CV.CvEnum.THRESH.CV_THRESH_BINARY, 15, new Gray(-7));
             //result = combine(filtered); return result;
             //Emgu.CV.Contour<System.Drawing.Point> c = filtered.FindContours();
             Emgu.CV.Contour<System.Drawing.Point> c =  filtered.FindContours(Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE, Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_CCOMP);
             //var c = filtered.FindContours( .FindContours(Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE, Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_CCOMP);
            



            List<Ellipse> ellipses = new List<Ellipse>();
            var ThomsonReutersBrandColor = new Bgr(31, 137, 246);
            
            while (c != null)
            {
                if (c.Count() >= _min_contour_count)
                {
                    System.Drawing.PointF[] mypoints = Array.ConvertAll(
                      c.ToArray<System.Drawing.Point>(),
                      value => new System.Drawing.PointF(value.X, value.Y)
                    );

                    Ellipse e = Emgu.CV.PointCollection.EllipseLeastSquareFitting(mypoints);
                    MCvBox2D box = e.MCvBox2D;
                    box.size.Height *= 0.5f;
                    box.size.Width *= 0.5f;
                    Ellipse final_ellipse = new Ellipse(box);

                    DenseMatrix m = DenseMatrix.CreateDiagonal(3, 3, 1);
                    double a = final_ellipse.MCvBox2D.size.Width;
                    double b = final_ellipse.MCvBox2D.size.Height;
                    double s = a / b;
                    DenseMatrix scale = DenseMatrix.CreateDiagonal(3, 3, 1);
                    scale[0, 0] = s;
                    double angle = final_ellipse.MCvBox2D.angle / 180 * Math.PI;
                    m[0, 0] = Math.Cos(angle);
                    m[0, 1] = -Math.Sin(angle);
                    m[0, 2] = final_ellipse.MCvBox2D.center.X;
                    m[1, 0] = Math.Sin(angle);
                    m[1, 1] = Math.Cos(angle);
                    m[1, 2] = final_ellipse.MCvBox2D.center.Y;

                    m = scale * m;
                    Matrix<double> inv = m.Inverse();
                    List<double> ratings = new List<double>();


                    double width = image.Width;
                    double height = image.Height;

                    foreach (System.Drawing.PointF p in mypoints)
                    {
                        DenseVector x = new DenseVector(new double[] { p.X, p.Y, 1 });
                        Matrix<double> invX = (inv.Multiply(x.ToColumnMatrix()));

                        IEnumerable<Tuple<int, Vector<double>>> columnsInvX = invX.EnumerateColumnsIndexed(0, 1);
                        Vector<double> r = columnsInvX.ElementAt(0).Item2;
                        
                        r = r.Normalize(2);
                        Vector<Double> closest = r * a;

                        Matrix<double> mClosest = (m.Multiply(closest.ToColumnMatrix()));
                        IEnumerable<Tuple<int, Vector<double>>> columnsMClosest = mClosest.EnumerateColumnsIndexed(0, 1);
                        Vector<Double> closest_in_world = columnsMClosest.ElementAt(0).Item2;


                        System.Drawing.Point mypoint = new System.Drawing.Point((int)closest_in_world[0], (int)closest_in_world[1]);
                        /*if (mypoint.X >= 0 && mypoint.Y >= 0 && mypoint.X < width && mypoint.Y < height)
                        {
                            //image[mypoint.Y, mypoint.X] = new Bgr(0, 0, 255);
                            image[(int)p.Y, (int)p.X] = new Bgr(0, 255, 255);
                        }*/

                        Vector<Double> distance_in_world = closest_in_world - x;
 
                        ratings.Add(distance_in_world.Norm(2));
                    }

                    ratings.Sort();
                    double rating;
                    if (ratings.Count % 2 == 0)
                    {
                        rating = (ratings[ratings.Count / 2] + ratings[ratings.Count / 2 + 1]) * 0.5;
                    }
                    else
                    {
                        rating = ratings[(ratings.Count + 1) / 2];
                    }
                    if (rating < _distance_threshold)
                    {
                        ellipses.Add(final_ellipse);
                    }


                }
                c = c.HNext;
            }

            ellipses.Sort(
              (a, b) =>
              {
                  double dista = a.MCvBox2D.center.X * a.MCvBox2D.center.X + a.MCvBox2D.center.Y * a.MCvBox2D.center.Y;
                  double distb = b.MCvBox2D.center.X * b.MCvBox2D.center.X + b.MCvBox2D.center.Y * b.MCvBox2D.center.Y;
                  return dista.CompareTo(distb);
              }
            );

            Bgr bgr = ThomsonReutersBrandColor;
            MCvFont f = new MCvFont(Emgu.CV.CvEnum.FONT.CV_FONT_HERSHEY_PLAIN, 0.8, 0.8);
            int count = 1;
            foreach (Ellipse e in ellipses)
            {
                image.Draw(e, bgr, 2);
                image.Draw(count.ToString(), ref f, new System.Drawing.Point((int)e.MCvBox2D.center.X, (int)e.MCvBox2D.center.Y), bgr);
                count++;
            }

            MCvFont font = new MCvFont(Emgu.CV.CvEnum.FONT.CV_FONT_HERSHEY_PLAIN, 5.0, 5.0);
            font.thickness = 3;
            image.Draw("Hits: " + ellipses.Count.ToString(), ref font, new Point(100, 100), ThomsonReutersBrandColor);

            return result;
        }
    }
}