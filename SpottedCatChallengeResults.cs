﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace SpottedCatChallenge
{
    [Serializable]
    [XmlRoot]
    public class SpottedCatChallengeResults
    {
        private DetectionParameters detectionParameters;
        private List<SpottedCatChallengeResult> results;

        public SpottedCatChallengeResults()
        {
            detectionParameters = new SpottedCatChallenge.DetectionParameters();
            results = new List<SpottedCatChallengeResult>();
        }

        public DetectionParameters DetectionParameters
        {
            get { return detectionParameters; }
            set { detectionParameters = value; }
        }

        public List<SpottedCatChallengeResult> Results
        {
            get { return results; }
            set { results = value; }
        }

        public void Sort()
        {
            results.Sort(new SpottedCatResultComparer());
        }
    }
}
