﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SpottedCatChallenge
{
    public partial class FormSettings : Form
    {
        public FormSettings()
        {
            InitializeComponent();
        }

        public void DetectorParametersToUI(DetectionParameters detectorParams) 
        {
            this.sensitivityUpDown.Value = detectorParams.Sensitivity;
            this.chkboxGenDebugImgsPOS.Checked = detectorParams.GenerateDebugImagesPOS;
            this.chkboxGenDebugImgsNEG.Checked = detectorParams.GenerateDebugImagesNEG;
            this.chkboxOverrideFolder.Checked = detectorParams.OverrideOutputFolder;
        }

        public void UItoDetectionParameters(ref DetectionParameters detectorParams)
        {
            detectorParams.Sensitivity = (int)this.sensitivityUpDown.Value;
            detectorParams.GenerateDebugImagesPOS = this.chkboxGenDebugImgsPOS.Checked ;
            detectorParams.GenerateDebugImagesNEG = this.chkboxGenDebugImgsNEG.Checked;
            detectorParams.OverrideOutputFolder = this.chkboxOverrideFolder.Checked;
        }


        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void btnOK_Click(object sender, EventArgs e)
        {

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            
        }
    }
}
