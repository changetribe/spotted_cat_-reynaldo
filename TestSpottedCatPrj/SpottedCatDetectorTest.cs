﻿using SpottedCatChallenge;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Emgu.CV.Structure;
using Emgu.CV;
using System.IO;
using System.Collections.Generic;

namespace TestSpottedCatPrj
{
    
    
    /// <summary>
    ///Some OLD unit testing (it does not hurt anyway :)
    ///
    ///</summary>
    [TestClass()]
    public class SpottedCatDetectorTest
    {
        // NOTE: C:\EMGU\BIN was added to Windows path
        private TestContext testContextInstance;

        private Dictionary<string, string> imagesTest;

        public SpottedCatDetectorTest()
        {
            imagesTest = new Dictionary<string, string>();
            imagesTest.Add("107210424230124423214414482195189135250_2.jpg", "zebra");
            imagesTest.Add("sample2.jpg", "cheetah nearby");
            imagesTest.Add("box_in_scene.png", "boxes");
            imagesTest.Add("10133052659369446788107695243109033091_0.jpg", "hyena");
            imagesTest.Add("101710936845483432573822999089226384775_2.jpg", "landscape");
            imagesTest.Add("99918492929935310734214655567859418444_2.jpg", "cheetah leaving");
            imagesTest.Add("104795964304951838501616404708546057546_1.jpg", "cheetah very close");
            imagesTest.Add("sample1.jpg", "undetected c h e e t a h muy cerca");
            imagesTest.Add("115872033954632641344316139114474918971_2.jpg", "cheetah");
            imagesTest.Add("box_in_scene-new.png", "cheetah");
            imagesTest.Add("105426507922298233889447262088403777181_0.jpg", "cheetah piece");
            imagesTest.Add("10244797732909875767514294212816901970_0.jpg", "cheetah with high flash");
        }

        /// <summary>
        ///Obtiene o establece el contexto de la prueba que proporciona
        ///la información y funcionalidad para la ejecución de pruebas actual.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Atributos de prueba adicionales
        // 
        //Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
        //
        //Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize para ejecutar código antes de ejecutar cada prueba
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        private string getImagePath(string image)
        {
            return Path.GetFullPath(Path.Combine(TestContext.DeploymentDirectory, @"../../../../../bin/" + image));
        }

        private DetectionParameters getDetectorParams1()
        {
            
            DetectionParameters detectorParams1 = new DetectionParameters();
            detectorParams1.IsDiagnoseMode = false;
            detectorParams1.PositiveModels.Add(getImagePath("cheetah2.png"));
            detectorParams1.PositiveModels.Add(getImagePath("cheetah1.png"));
            return detectorParams1;
        }

        

        /// <summary>
        ///Test IsDiagnoseMode
        ///</summary>
        [TestMethod()]
        public void IsDiagnoseModeTest()
        {
            SpottedCatDetector target = new SpottedCatDetector(getDetectorParams1());
            bool expected = false;
            bool actual;
            target.IsDiagnoseMode = expected;
            actual = target.IsDiagnoseMode;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///Test MatchedFeatures
        ///</summary>
        [TestMethod()]
        public void MatchedFeaturesTest()
        {
            SpottedCatDetector target = new SpottedCatDetector(getDetectorParams1()); 
            int expected = 12; 
            int actual;

            string obsImg = getImagePath("sample2.jpg"); // cheetah cerca
            Image<Bgr, Byte> observedImageOrig = new Image<Bgr, byte>(obsImg);
            double probability = target.ImageIsSpottedCat(observedImageOrig, true);
            actual = target.MatchedFeatures;
            Assert.AreEqual(expected, actual, "Expected to find 12 features in image ");
        }

        /// <summary>
        ///Una prueba de DiagnoseModeResultImg
        ///</summary>
        [TestMethod()]
        public void DiagnoseModeResultImgTest()
        {
            DetectionParameters paramsdebug = getDetectorParams1();
            paramsdebug.IsDiagnoseMode = true;
            SpottedCatDetector target = new SpottedCatDetector(paramsdebug);

            string obsImg = getImagePath("sample2.jpg"); // cheetah cerca
            Image<Bgr, Byte> observedImageOrig = new Image<Bgr, byte>(obsImg);
            double probability = target.ImageIsSpottedCat(observedImageOrig, true);
            Assert.IsNotNull(target.DiagnoseModeResultImg, "Diagnose image should be not null");
        }

        /// <summary>
        ///Test 1 ImageIsSpottedCat
        ///</summary>
        [TestMethod()]
        public void ImageIsSpottedCatTest1()
        {
            foreach (KeyValuePair<string, string> p in imagesTest) {
                SpottedCatDetector target = new SpottedCatDetector(getDetectorParams1());
                string obsImg = getImagePath(p.Key);
                Image<Bgr, byte> targetImg = new Image<Bgr, byte>(obsImg);
                double expected = 0.8;
                double actual;
                actual = target.ImageIsSpottedCat(targetImg, true);
                if (p.Value.Contains("cheetah"))
                {
                    Assert.IsTrue(actual >= expected, p.Key + " is a spotted cat, thus expected >= 0.8");
                }
                else
                {
                    Assert.IsTrue(actual < expected, p.Key + " is NOT a spotted cat, thus expected < 0.8");
                }
            
            }
        }
    }
}
