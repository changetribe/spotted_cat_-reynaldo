﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace SpottedCatChallenge
{
    public class ImageGroup 
    {
        private List<string> images;

        public ImageGroup()
        { 
            images = new List<string>();
        }

        public List<string> Images
        {
          get { return images; }
          set { images = value; }
        }

        public void AddImage(string filename)
        {
            images.Add(filename);
        }

        public bool IsUnitarySet 
        {
            get { return images.Count <= 1; }
        }

        public void Sort()
        {
            images.Sort(new FileNamesSeqComparer());
        }
    }

    class FileInfoComparer : IComparer<FileInfo>
    {
        public int Compare(FileInfo x, FileInfo y)
        {
            string xName = Path.GetFileNameWithoutExtension(x.FullName);
            string yName = Path.GetFileNameWithoutExtension(y.FullName);

            return xName.CompareTo(yName);
        }
    }

    class FileNamesSeqComparer : IComparer<string>
    {
        private char[] seq_sep = { '-', '_' };

        public string GetNameWithoutSequenceNo(string fullname) 
        {
            string name = Path.GetFileNameWithoutExtension(fullname);
            int idx = name.LastIndexOfAny(seq_sep);
            string res = (idx > 0) ? name.Substring(0, idx) : name;
            return res;
        }

        private int TryToDetectImageSequenceNo(string fullname)
        {
            string name = Path.GetFileNameWithoutExtension(fullname);
            int idx = name.LastIndexOfAny(seq_sep);
            int seqNo = -1;
            string seqStr = "";

            if (idx > 0)
            {
                seqStr = name.Substring(idx + 1);
                Int32.TryParse(seqStr, out seqNo);
            }
            return seqNo;
        }

        public int Compare(string x, string y)
        {
            int seqX = TryToDetectImageSequenceNo(x);
            int seqY = TryToDetectImageSequenceNo(y);
            return seqX.CompareTo(seqY);
        }
    }

    /// <summary>
    /// Encapsulate knowledge on how to group sequence of images into groups (I am using sequence number in filenames, 
    /// but could for example use last write date (in picture's metadata) to group sequences instead
    /// </summary>
    public class ImageSequenceGrouping
    {
        private IList<ImageGroup> imageGroups;
        //private TimeSpan maxTimeSlotForGroup;
        private ImageGroup imgGroup = null;
        private FileNamesSeqComparer helper;


        public ImageSequenceGrouping()
        {
            imageGroups = new List<ImageGroup>();
            helper = new FileNamesSeqComparer();
            //maxTimeSlotForGroup = new TimeSpan(0, 0, 5); // 5 seconds FIXME ??  (Group images with creation timespan lying in a 5 second window)
        }

        public IList<ImageGroup> ImageGroups
        {
            get { return imageGroups; }
            set { imageGroups = value; }
        }

        public void ProcessFiles(List<FileInfo> files) 
        {
            FileInfo lastFileInfo = null;
            files.Sort(new FileInfoComparer());


            foreach (FileInfo file in files)
            {
                if (lastFileInfo == null)
                {
                    // First file
                    lastFileInfo = file;
                    imgGroup = new ImageGroup();
                    imgGroup.AddImage(file.FullName);
                    imageGroups.Add(imgGroup);
                    continue;
                }
                else 
                {
                    if (helper.GetNameWithoutSequenceNo(lastFileInfo.FullName).Equals(helper.GetNameWithoutSequenceNo(file.FullName)))
                    {
                        // assign to pre-existing image group
                        imgGroup.AddImage(file.FullName);
                    }
                    else 
                    {
                        // Create a new group
                        imgGroup = new ImageGroup();
                        imgGroup.AddImage(file.FullName);
                        imageGroups.Add(imgGroup);
                    }
                    lastFileInfo = file;
                }
            }

            // sort all groups
            foreach (ImageGroup group in imageGroups)
            {
                group.Sort();
            }
        }
    }
}
