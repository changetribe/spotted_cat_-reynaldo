﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpottedCatChallenge
{
    [Serializable]
    public class DetectionParameters
    {
        private bool isDiagnoseMode;
        private List<string> positiveModels;
        private List<string> negativeModels;

        private bool overrideOutputFolder;
        private bool generateDebugImagesPOS;
        private bool generateDebugImagesNEG;
        private int sensitivity;

        public int Sensitivity
        {
            get { return sensitivity; }
            set { sensitivity = value; }
        }


        public bool GenerateDebugImagesPOS
        {
            get { return generateDebugImagesPOS; }
            set { generateDebugImagesPOS = value; }
        }
        public bool GenerateDebugImagesNEG
        {
            get { return generateDebugImagesNEG; }
            set { generateDebugImagesNEG = value; }
        }


        public bool OverrideOutputFolder
        {
            get { return overrideOutputFolder; }
            set { overrideOutputFolder = value; }
        }

        public DetectionParameters()
        {
            positiveModels = new List<string>();
            negativeModels = new List<string>();
        }
        
        public bool IsDiagnoseMode
        {
            get { return isDiagnoseMode; }
            set { isDiagnoseMode = value; }
        }


        public List<string> PositiveModels
        {
            get { return positiveModels; }
            set { positiveModels = value; }
        }

        public List<string> NegativeModels
        {
            get { return negativeModels; }
            set { negativeModels = value; }
        }
    }
}
