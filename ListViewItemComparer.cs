﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Windows.Forms;

namespace SpottedCatChallenge
{
    // Implements the manual sorting of items by column.
    class ListViewItemComparer : IComparer
    {
        private int col;
        public ListViewItemComparer()
        {
            col = 1;
        }
        public ListViewItemComparer(int column)
        {
            col = column;
        }
        public int Compare(object x, object y)
        {
            int returnVal = -1;
            Double probx = (Double) ((ListViewItem)x).SubItems[col].Tag;
            Double proby = (Double)((ListViewItem)y).SubItems[col].Tag;

            if(probx == proby) {
                returnVal = 0;
            } else {
                returnVal = (probx > proby) ? -1 : 1;
            }

            return returnVal;
        }
    }
}
