﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Features2D;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using Emgu.CV.Util;
using Emgu.CV.GPU;


using System.Drawing;
using System.IO;
using MathNet.Numerics.LinearAlgebra.Double;

namespace SpottedCatChallenge
{
    public class ImageInfo
    {
        public Image<Gray, byte> ImgGray { get; set; }
        public VectorOfKeyPoint Keypoints { get; set; }
        public Matrix<byte> Descriptors { get; set; }

        public ImageInfo(Image<Gray, byte> imgGray, VectorOfKeyPoint keypoints, Matrix<byte> descriptors)
        {
            this.ImgGray = imgGray;
            this.Keypoints = keypoints;
            this.Descriptors = descriptors;
        }
    }

    public class SpottedCatDetector
    {
        // Private members
        private DetectionParameters detectionParameters;
        private Dictionary<string, Image<Bgr, byte>> positiveModels;
        private Dictionary<string, ImageInfo> positiveModelsInfo; // calculated in PrepareFeatureDetection
        private Dictionary<string, Image<Bgr, byte>> negativeModels;
        
        private SpottedCatChallengeInput spottedCatChallengeInput;
        private SpottedCatChallengeResults detectionResults;
        private ImageCollectionMatcher imgCollectionMatcher;

        private float[,] loGKernelSigma14 = {
                                            {0,0,3,2,2,2,3,0,0},
                                            {0,2,3,5,5,5,3,2,0},
                                            {3,3,5,3,0,3,5,3,3},
                                            {2,5,3,-12,-23,-12,3,5,2},
                                            {2,5,0,-23,-40,-23,0,5,2},
                                            {2,5,3,-12,-23,-12,3,5,2},
                                            {3,3,5,3,0,3,5,3,3},
                                            {0,2,3,5,5,5,3,2,0},
                                            {0,0,3,2,2,2,3,0,0},
                                            };

        private Image<Bgr, Byte> diagnoseModeResultImg;
        private int matchedFeatures;
        private Bgr ThomsonReutersBrandColor = new Bgr(31, 137, 246);

        // Public delegates definition
        public delegate void OnProgressStepEventHandler(EventArgs e, ref bool detectorShouldContinue);
        // return string = file path to be processed
        public delegate void OnBeforeProcessImageEventHandler(EventArgs e, string path);
        public delegate void OnFinishProcessImageEventHandler(EventArgs e, SpottedCatDetector detector, string path, double probability, bool isSpottedCat);
        public delegate void OnErrorProcessImageEventHandler(EventArgs e, string path, string errorMessage);
        public delegate void OnDetectionCancelledEventHandler(EventArgs e);

        // Public events (useful for user interaction / awareness)
        public event OnProgressStepEventHandler OnProgressStep;
        public event OnBeforeProcessImageEventHandler OnBeforeProcessImage;
        public event OnFinishProcessImageEventHandler OnFinishProcessImage;
        public event OnErrorProcessImageEventHandler OnErrorProcessImage;
        public event OnDetectionCancelledEventHandler OnDetectionCancelled;

        public bool IsDiagnoseMode
        {
            get { return detectionParameters.IsDiagnoseMode; }
            set { detectionParameters.IsDiagnoseMode = value; }
        }

        public Image<Bgr, Byte> DiagnoseModeResultImg
        {
            get { return diagnoseModeResultImg; }
            set { diagnoseModeResultImg = value; }
        }

        public int MatchedFeatures
        {
            get { return matchedFeatures; }
            set { matchedFeatures = value; }
        }



        // Constructor
        public SpottedCatDetector(DetectionParameters detectionParameters)
        {
            diagnoseModeResultImg = null;
            positiveModels = new Dictionary<string, Image<Bgr, byte>>();
            negativeModels = new Dictionary<string, Image<Bgr, byte>>();
            this.detectionParameters = detectionParameters;

            foreach (string positiveModel in detectionParameters.PositiveModels)
            {
                positiveModels.Add(positiveModel, new Image<Bgr, byte>(positiveModel));
            }
            foreach (string negativeModel in detectionParameters.NegativeModels)
            {
                negativeModels.Add(negativeModel, new Image<Bgr, byte>(negativeModel));
            }
        }



        public SpottedCatChallengeResults DetectSpottedCats(SpottedCatChallengeInput spottedCatChallengeInput)
        {
            this.spottedCatChallengeInput = spottedCatChallengeInput;

            detectionResults = new SpottedCatChallengeResults();
            detectionResults.DetectionParameters = this.detectionParameters;
            List<FileInfo> inputFiles = spottedCatChallengeInput.InputFiles;

            // Prepare Feature Detection
            this.PrepareFeatureDetection();

            // I think it is important to use the fact that images come as group of sequences most of the time
            // Removing background is critical
            // On the other hand, single images are treated as unitary sets
            ImageSequenceGrouping imageSequenceGrouping = new ImageSequenceGrouping();
            imageSequenceGrouping.ProcessFiles(inputFiles);

            List<ImageGroup> singleImages = imageSequenceGrouping.ImageGroups.Where(group => group.IsUnitarySet).ToList();
            List<ImageGroup> imageSequences = imageSequenceGrouping.ImageGroups.Where(group => !group.IsUnitarySet).ToList();

            ProcessImageSequences(imageSequences);

            ProcessSingleImages(singleImages);

            return detectionResults;
        }

        private void checkDetectorShouldContinue(bool detectorShouldContinue) 
        {
            if (!detectorShouldContinue) 
            {
                if (OnDetectionCancelled != null) 
                {
                    OnDetectionCancelled(null);
                } 
            }
        }

        private void ProcessImageSequences(List<ImageGroup> imageSequences)
        {
            int k = 0;
            int maxIdx = 0;
            string currFile = "";
            string nextFile = "";
            Image<Bgr, byte> currImg = null;
            Image<Bgr, byte> nextImg = null;
            double probability = 0.0;
            bool isSpottedCat = false;
            bool detectorShouldContinue = true;


            // For all groups of images
            foreach (ImageGroup group in imageSequences)
            {
                checkDetectorShouldContinue(detectorShouldContinue);
                if (!detectorShouldContinue) break;
                
                ImageBackgroundSubstractor imageBackgroundSubstractor = new ImageBackgroundSubstractor();
                maxIdx = group.Images.Count;
                // for all images in each group
                for (k = 0; k < maxIdx - 1; k++)
                {
                    try
                    {
                        currFile = group.Images[k];
                        nextFile = group.Images[k + 1];
                        currImg = new Image<Bgr, byte>(currFile);
                        nextImg = new Image<Bgr, byte>(nextFile);


                        imageBackgroundSubstractor.SubstractBackground(currImg, nextImg);
                        using (Image<Bgr, byte> foreCurr = imageBackgroundSubstractor.Img1Masked)
                        using (Image<Bgr, byte> foreNext = imageBackgroundSubstractor.Img2Masked)
                        {
                            probability = ImageIsSpottedCat(foreCurr, null, false);
                            probability = Math.Max(probability, ImageIsSpottedCat(foreNext, null, false));
                            isSpottedCat = (probability >= spottedCatChallengeInput.ConfidenceLevelZeroToOne);
                            if (isSpottedCat) break;
                        }
                    }
                    catch (Exception excpt)
                    {
                        if (OnErrorProcessImage != null) { OnErrorProcessImage(null, currFile + "-" + nextImg, excpt.Message); }
                    }
                }
                
                // iterate through all images in the group
                foreach (string file in group.Images) 
                {
                    try
                    {
                        if (OnBeforeProcessImage != null) { OnBeforeProcessImage(null, file); }

                        SpottedCatChallengeResult singleResult = new SpottedCatChallengeResult(file, this.matchedFeatures, 
                            probability, isSpottedCat);
                        detectionResults.Results.Add(singleResult);

                        if (OnFinishProcessImage != null) { OnFinishProcessImage(null, this, file, probability, isSpottedCat); }


                        if (OnProgressStep != null) 
                        { 
                            OnProgressStep(null, ref detectorShouldContinue);
                            if(!detectorShouldContinue) break;
                        }
                    }
                    catch (Exception excpt) 
                    {
                        if (OnErrorProcessImage != null) { OnErrorProcessImage(null, file, excpt.Message); }
                    }
                }
            }
        }

        private void ProcessSingleImages(List<ImageGroup> singleImages)
        {
            string obsImg;
            bool detectorShouldContinue = true;

            // Process individual files
            foreach (ImageGroup group in singleImages)
            {
                obsImg = group.Images[0];
                try
                {
                    using (Image<Bgr, Byte> observedImageOrig = new Image<Bgr, byte>(obsImg))
                    using (Image<Bgr, float> observedImageOrigFloat = new Image<Bgr, float>(obsImg))
                    {
                        detectSingleImg(obsImg, observedImageOrig, observedImageOrigFloat, ref detectorShouldContinue);
                        checkDetectorShouldContinue(detectorShouldContinue);
                        if (!detectorShouldContinue) break;
                    }
                }
                catch (Exception excpt) 
                {
                    if (OnErrorProcessImage != null) { OnErrorProcessImage(null, obsImg, excpt.Message); }
                }
            }
        }

        public void detectSingleImg(string obsImg, 
            Image<Bgr, byte> observedImage, 
            Image<Bgr, float> observedImageFloat, ref bool detectorShouldContinue)
        {
            bool isSpottedCat = false; // being a little pessimistic :)
            
            try
            {
                // Before process image event
                if (OnBeforeProcessImage != null) { OnBeforeProcessImage(null, obsImg); }

                double probability = this.ImageIsSpottedCat(observedImage, observedImageFloat, true);
                isSpottedCat = (probability >= spottedCatChallengeInput.ConfidenceLevelZeroToOne);
                SpottedCatChallengeResult singleResult = new SpottedCatChallengeResult(obsImg, this.matchedFeatures, probability, isSpottedCat);
                detectionResults.Results.Add(singleResult);

                if (OnFinishProcessImage != null) { OnFinishProcessImage(null, this, obsImg, probability, isSpottedCat); }

                if (OnProgressStep != null) 
                { 
                    OnProgressStep(null, ref detectorShouldContinue);
                }
            }
            catch (Exception excpt) {
                if (OnErrorProcessImage != null) { OnErrorProcessImage(null, obsImg, excpt.Message); }
            }
        }

        
        /// <summary>
        /// Laplacian of a Gaussian (LoG), then Adaptative Threshold and find contours enclosing a box of certain size ranges
        /// BTW: I am trained as a Physicist, and this is a good way to detect "standard large particles" (I mean just newtonian particles, nothing related to QM
        /// TPR is not so high for this algorithm, but its sensitivity is great, so FPR should be really low
        /// </summary>
        /// <param name="observedImage"></param>
        /// <returns></returns>
        private double DetectImageLoG(Image<Bgr, byte> observedImage)
        {
            double result = 0;
            int countNo = 0;
            try
            {
                Image<Gray, Byte> observedImageGray = observedImage.Convert<Gray, Byte>().PyrDown().PyrUp();
                observedImageGray._EqualizeHist();
                
                // Apply Laplacian of a Gaussian (LoG) convolution kernel
                observedImageGray = observedImageGray.Convolution(new ConvolutionKernelF(loGKernelSigma14)).Convert<Gray, Byte>();

                int morphOpSize = 25;
                StructuringElementEx element = new StructuringElementEx(morphOpSize, morphOpSize, morphOpSize / 2, morphOpSize / 2, CV_ELEMENT_SHAPE.CV_SHAPE_ELLIPSE);

                Image<Gray, byte> filtered = observedImageGray.MorphologyEx(element, CV_MORPH_OP.CV_MOP_TOPHAT, 3);
                filtered = filtered.SmoothGaussian(25);

                filtered = filtered.ThresholdAdaptive(new Gray(255), ADAPTIVE_THRESHOLD_TYPE.CV_ADAPTIVE_THRESH_GAUSSIAN_C, THRESH.CV_THRESH_BINARY, 25, new Gray(-3));

                Contour<Point> contours = filtered.FindContours(CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE, RETR_TYPE.CV_RETR_CCOMP);

                Contour<Point> cont = contours;
                if (IsDiagnoseMode)
                {
                    diagnoseModeResultImg = observedImage.Clone();
                }

                while (cont != null)
                {
                    Contour<Point> next = cont.HNext;
                    cont.HNext = null;

                    MCvBox2D minAreaRect = cont.GetMinAreaRect();
                    Rectangle rect = cont.BoundingRectangle;
                    // Approximately 'round' . My concept of 'round' might be a bit fuzzy anyway... :)
                    if ((minAreaRect.size.Height <= 1.3 * minAreaRect.size.Width) && (minAreaRect.size.Height >= 0.7 * minAreaRect.size.Width) && (minAreaRect.size.Height * minAreaRect.size.Width > 25))
                    {
                        countNo++;
                        if (IsDiagnoseMode)
                        {
                            diagnoseModeResultImg.Draw(rect, new Bgr(Color.Red), 1);
                            CvInvoke.cvDrawContours(
                                diagnoseModeResultImg, cont, new MCvScalar(0, 255, 0), new MCvScalar(0, 255, 0), 2, 2, LINE_TYPE.EIGHT_CONNECTED, new Point(0, 0));
                        }
                    }

                    cont = next;
                }
                if (IsDiagnoseMode)
                {
                    MCvFont font = new MCvFont(FONT.CV_FONT_HERSHEY_PLAIN, 5.0, 5.0);
                    font.thickness = 3;
                    diagnoseModeResultImg.Draw("Hits: " + countNo.ToString(), ref font, new Point(100, 100), new Bgr(Color.Red));
                }
            }
            catch (Exception e) { }

            result = Math.Min((double)countNo / 50, 1.0);

            return result;
        }

        private double DetectImageLoG2GOOD(Image<Bgr, byte> observedImage)
        {
            double result = 0;
            int countNo = 0;

            try
            {
                Image<Gray, Byte> observedImageGray = observedImage.Convert<Gray, Byte>().PyrDown().PyrUp();
                observedImageGray._EqualizeHist();

                // Apply Laplacian of a Gaussian (LoG) convolution kernel
                observedImageGray = observedImageGray.Convolution(new ConvolutionKernelF(loGKernelSigma14)).Convert<Gray, Byte>();

                int morphOpSize = 55;//31//31//55//31
                StructuringElementEx element = new StructuringElementEx(morphOpSize, morphOpSize, morphOpSize / 2, morphOpSize / 2, CV_ELEMENT_SHAPE.CV_SHAPE_ELLIPSE);

                Image<Gray, byte> filtered = observedImageGray.MorphologyEx(element, CV_MORPH_OP.CV_MOP_TOPHAT, 3);
                //filtered = filtered.SmoothGaussian(3);//5//17//19//25

                filtered = filtered.ThresholdAdaptive(new Gray(255), ADAPTIVE_THRESHOLD_TYPE.CV_ADAPTIVE_THRESH_GAUSSIAN_C, THRESH.CV_THRESH_BINARY, 25, new Gray(-3));

                Contour<Point> contours = filtered.FindContours(CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE, RETR_TYPE.CV_RETR_CCOMP);

                Contour<Point> cont = contours;
                if (IsDiagnoseMode)
                {
                    diagnoseModeResultImg = observedImage.Clone();
                }

                while (cont != null)
                {
                    Contour<Point> next = cont.HNext;
                    cont.HNext = null;

                    MCvBox2D minAreaRect = cont.GetMinAreaRect();
                    Rectangle rect = cont.BoundingRectangle;
                    // Approximately 'round' :)
                    //if ((minAreaRect.size.Height <= 1.4 * minAreaRect.size.Width) && (minAreaRect.size.Height >= 0.6 * minAreaRect.size.Width) && (minAreaRect.size.Height * minAreaRect.size.Width > 20))
                    {
                        countNo++;
                        if (IsDiagnoseMode)
                        {
                            diagnoseModeResultImg.Draw(rect, new Bgr(Color.Red), 1);
                            CvInvoke.cvDrawContours(
                                diagnoseModeResultImg, cont, new MCvScalar(0, 255, 0), new MCvScalar(0, 255, 0), 2, 2, LINE_TYPE.EIGHT_CONNECTED, new Point(0, 0));
                        }
                    }

                    cont = next;
                }
                if (IsDiagnoseMode)
                {
                    MCvFont font = new MCvFont(FONT.CV_FONT_HERSHEY_PLAIN, 5.0, 5.0);
                    font.thickness = 3;
                    diagnoseModeResultImg.Draw("Hits: " + countNo.ToString(), ref font, new Point(100, 100), new Bgr(Color.Red));
                }
            }
            catch (Exception e) { }

            result = Math.Min((double)countNo / 30, 1.0);

            return result;
        }


        private double DetectImageLoG2(Image<Bgr, byte> observedImage)
        {
            double result = 0;
            int countNo = 0;

            try
            {
                Image<Gray, Byte> observedImageGray = observedImage.Convert<Gray, Byte>().PyrDown().PyrUp();
                observedImageGray._EqualizeHist();

                // Apply Laplacian of a Gaussian (LoG) convolution kernel
                observedImageGray = observedImageGray.Convolution(new ConvolutionKernelF(loGKernelSigma14)).Convert<Gray, Byte>();

                int morphOpSize = 55;//31//31//55//31
                StructuringElementEx element = new StructuringElementEx(morphOpSize, morphOpSize, morphOpSize / 2, morphOpSize / 2, CV_ELEMENT_SHAPE.CV_SHAPE_ELLIPSE);

                Image<Gray, byte> filtered = observedImageGray.MorphologyEx(element, CV_MORPH_OP.CV_MOP_TOPHAT, 3);
                filtered = filtered.SmoothGaussian(3);//5//17//19//25

                filtered = filtered.ThresholdAdaptive(new Gray(255), ADAPTIVE_THRESHOLD_TYPE.CV_ADAPTIVE_THRESH_GAUSSIAN_C, THRESH.CV_THRESH_BINARY, 25, new Gray(-3));

                Contour<Point> contours = filtered.FindContours(CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE, RETR_TYPE.CV_RETR_CCOMP);

                Contour<Point> cont = contours;
                if (IsDiagnoseMode)
                {
                    diagnoseModeResultImg = observedImage.Clone();
                }

                while (cont != null)
                {
                    Contour<Point> next = cont.HNext;
                    cont.HNext = null;

                    MCvBox2D minAreaRect = cont.GetMinAreaRect();
                    Rectangle rect = cont.BoundingRectangle;
                    // Approximately 'round' :)
                    if ((minAreaRect.size.Height <= 1.5 * minAreaRect.size.Width) && (minAreaRect.size.Height >= 0.5 * minAreaRect.size.Width) && (minAreaRect.size.Height * minAreaRect.size.Width > 10))
                    {
                        countNo++;
                        if (IsDiagnoseMode)
                        {
                            diagnoseModeResultImg.Draw(rect, new Bgr(Color.Red), 1);
                            CvInvoke.cvDrawContours(
                                diagnoseModeResultImg, cont, new MCvScalar(0, 255, 0), new MCvScalar(0, 255, 0), 2, 2, LINE_TYPE.EIGHT_CONNECTED, new Point(0, 0));
                        }
                    }

                    cont = next;
                }
                if (IsDiagnoseMode)
                {
                    MCvFont font = new MCvFont(FONT.CV_FONT_HERSHEY_PLAIN, 5.0, 5.0);
                    font.thickness = 3;
                    diagnoseModeResultImg.Draw("Hits: " + countNo.ToString(), ref font, new Point(100, 100), new Bgr(Color.Red));
                }
            }
            catch (Exception e) { }

            result = Math.Min((double)countNo / 30, 1.0);

            return result;
        }

        /// <summary>
        /// TopHatMorphologyGoodDetector with ADAPTATIVE threshold
        /// </summary>
        /// <param name="modelImage"></param>
        /// <param name="observedImage"></param>
        /// <returns></returns>
        private double DetectImageTopHatMorphologyAdaptativeThresholdGoodDetector(Image<Bgr, byte> observedImage)
        {
            double result = 0;
            int countNo = 0;
            try
            {
                Image<Gray, Byte> observedImageGray = observedImage.Convert<Gray, Byte>().PyrDown().PyrUp().Not();
                observedImageGray._EqualizeHist();
                observedImageGray = observedImageGray.SmoothMedian(13);

                int morphOpSize = 30;
                StructuringElementEx element = new StructuringElementEx(morphOpSize, morphOpSize, morphOpSize / 2, morphOpSize / 2, CV_ELEMENT_SHAPE.CV_SHAPE_ELLIPSE);

                Image<Gray, byte> filtered = observedImageGray.MorphologyEx(element, CV_MORPH_OP.CV_MOP_TOPHAT, 1);
                filtered = filtered.SmoothGaussian(19);

                filtered = filtered.ThresholdAdaptive(new Gray(255), ADAPTIVE_THRESHOLD_TYPE.CV_ADAPTIVE_THRESH_GAUSSIAN_C, THRESH.CV_THRESH_BINARY, 15, new Gray(-3));

                Contour<Point> contours = filtered.FindContours(CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE, RETR_TYPE.CV_RETR_CCOMP);

                Contour<Point> cont = contours;
                if (IsDiagnoseMode)
                {
                    diagnoseModeResultImg = observedImage.Clone();
                }

                while (cont != null)
                {
                    Contour<Point> next = cont.HNext;
                    cont.HNext = null;

                    MCvBox2D minAreaRect = cont.GetMinAreaRect();
                    Rectangle rect = cont.BoundingRectangle;
                    // Approximately 'round' :)
                    if ((minAreaRect.size.Height <= 1.3 * minAreaRect.size.Width) && (minAreaRect.size.Height >= 0.7 * minAreaRect.size.Width) && (minAreaRect.size.Height * minAreaRect.size.Width > 40))
                    {
                        countNo++;
                        if (IsDiagnoseMode)
                        {
                            diagnoseModeResultImg.Draw(rect, new Bgr(Color.Red), 1);
                            CvInvoke.cvDrawContours(
                                diagnoseModeResultImg, cont, new MCvScalar(0, 255, 0), new MCvScalar(0, 255, 0), 2, 2, LINE_TYPE.EIGHT_CONNECTED, new Point(0, 0));
                        }
                    }

                    cont = next;
                }
                if (IsDiagnoseMode)
                {
                    MCvFont font = new MCvFont(FONT.CV_FONT_HERSHEY_PLAIN, 5.0, 5.0);
                    font.thickness = 3;
                    diagnoseModeResultImg.Draw("Hits: " + countNo.ToString(), ref font, new Point(100, 100), new Bgr(Color.Red));
                }
            }
            catch (Exception e) { }

            result = Math.Min((double)countNo / 50, 1.0);

            return result;
        }


        private double DetectImagTopHatMorphologyGoodDetector(Image<Bgr, byte> observedImage)
        {
            double result = 0;
            int countNo = 0;
            try
            {
                Image<Gray, Byte> observedImageGray = observedImage.Convert<Gray, Byte>().PyrDown().PyrUp().Not();
                observedImageGray._EqualizeHist();
                observedImageGray = observedImageGray.SmoothMedian(9);

                int morphOpSize = 35;
                StructuringElementEx element = new StructuringElementEx(morphOpSize, morphOpSize, morphOpSize / 2, morphOpSize / 2, CV_ELEMENT_SHAPE.CV_SHAPE_ELLIPSE);

                Image<Gray, byte> filtered = observedImageGray.MorphologyEx(element, CV_MORPH_OP.CV_MOP_TOPHAT, 1);

                double thressh = CvInvoke.cvThreshold(filtered, filtered, 60, 200, THRESH.CV_THRESH_BINARY);

                Contour<Point> contours = filtered.FindContours(CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE, RETR_TYPE.CV_RETR_CCOMP);

                Contour<Point> cont = contours;
                if (IsDiagnoseMode)
                {
                    diagnoseModeResultImg = observedImage.Clone();
                }

                while (cont != null)
                {
                    Contour<Point> next = cont.HNext;
                    cont.HNext = null;

                    MCvBox2D minAreaRect = cont.GetMinAreaRect();
                    Rectangle rect = cont.BoundingRectangle;
                    // Approximately 'round' :)
                    if ((minAreaRect.size.Height <= 1.3 * minAreaRect.size.Width) && (minAreaRect.size.Height >= 0.7 * minAreaRect.size.Width)) //(minAreaRect.size.Height * minAreaRect.size.Width  > 4 ) &&  
                    {
                        countNo++;
                        if (IsDiagnoseMode)
                        {
                            diagnoseModeResultImg.Draw(rect, new Bgr(Color.Red), 1);
                            CvInvoke.cvDrawContours(
                                diagnoseModeResultImg, cont, new MCvScalar(0, 255, 0), new MCvScalar(0, 255, 0), 2, 2, LINE_TYPE.EIGHT_CONNECTED, new Point(0, 0));
                        }
                    }

                    cont = next;
                }
                if (IsDiagnoseMode)
                {
                    MCvFont font = new MCvFont(FONT.CV_FONT_HERSHEY_PLAIN, 5.0, 5.0);
                    font.thickness = 3;
                    diagnoseModeResultImg.Draw("Hits: " + countNo.ToString(), ref font, new Point(100, 100), new Bgr(Color.Red));
                }
            }
            catch (Exception e) { }

            result = Math.Min((double)countNo / 60, 1.0);

            return result;
        }

        /// <summary>
        /// DetectImageHoughTransform
        /// </summary>
        /// <param name="modelImage"></param>
        /// <param name="observedImage"></param>
        /// <returns></returns>
        private double DetectImageHoughTransform(Image<Bgr, byte> observedImage)
        {
            double result = 0;
            try
            {
                double cannyThreshold = 100;
                double circleAccumulatorThreshold = 120;

                Image<Gray, Byte> observedImageGray = observedImage.Convert<Gray, Byte>().PyrDown().PyrUp(); 
                //.Erode(2).Dilate(2).Canny(50,50);
                observedImageGray._EqualizeHist();
                //observedImageGray = observedImageGray.SmoothMedian(3).Canny(100, 120);
                //.Canny(50,50); //Convert observed image to grayscale
                
                // try to detect some "circles" in observed image
                CircleF[] circles = observedImageGray.HoughCircles(
                    new Gray(cannyThreshold),
                    new Gray(circleAccumulatorThreshold),
                    2, //Resolution of the accumulator used to detect centers of the circles
                    8.0, //min distance 
                    8, //min radius
                    50 //max radius
                    )[0]; //Get the circles from the first channel


                int countNo = circles.Length;


                if (IsDiagnoseMode)
                {
                    foreach (CircleF circle in circles)
                        observedImage.Draw(circle, new Bgr(Color.Red), 1);

                    //Image<Gray, Byte>[] channels = new Image<Gray, byte>[3];
                    //channels[0] = observedImageGray;
                    //channels[1] = observedImageGray;
                    //channels[2] = observedImageGray;

                    //diagnoseModeResultImg = new Image<Bgr, byte>(channels);
                    diagnoseModeResultImg = observedImage.Clone();

                    MCvFont font = new MCvFont(FONT.CV_FONT_HERSHEY_PLAIN, 5.0, 5.0);
                    font.thickness = 3;
                    diagnoseModeResultImg.Draw("Hits: " + countNo.ToString(), ref font, new Point(100, 100), new Bgr(Color.Red));
                }

                return Math.Min((double) countNo / 10, 1.0);
            }
            catch (Exception e)
            { }
            return result;
        }

        private double DetectImageTESTAdaptativeThreshold(Image<Bgr, byte> modelImage, Image<Bgr, byte> observedImage)
        {
            double result = 0;
            try
            {
                //Convert observed image to grayscale and de-noise
                Image<Gray, Byte> observedImageGray = observedImage.Convert<Gray, Byte>().PyrDown().PyrUp();
                // Equalize histogram
                observedImageGray._EqualizeHist();
                // Smooth Gaussian
                //observedImageGray = observedImageGray.SmoothGaussian(5);
                observedImageGray = observedImageGray.ThresholdAdaptive(new Gray(255), ADAPTIVE_THRESHOLD_TYPE.CV_ADAPTIVE_THRESH_GAUSSIAN_C, THRESH.CV_THRESH_BINARY, 15, new Gray(1));

                Image<Gray, Byte>[] channels = new Image<Gray, byte>[3];
                channels[0] = observedImageGray;
                channels[1] = observedImageGray;
                channels[2] = observedImageGray;
                diagnoseModeResultImg = new Image<Bgr, byte>(channels);
                result = DetectImageHoughTransform(diagnoseModeResultImg);
            }
            catch (Exception e)
            { }
            return result;
        }


        private double DetectImageEllipseDetector(Image<Bgr, byte> observedImage) 
        {
            Emgu.CV.Image<Bgr, byte> result = observedImage;
            int _min_contour_count = 40;
            double _distance_threshold = 1000.0;

            Emgu.CV.Image<Gray, Byte> observedImageGray = observedImage.Convert<Gray, Byte>().Not();
            observedImageGray._EqualizeHist();

             
             //observedImageGray = observedImageGray.SmoothGaussian(3);
             //observedImageGray = observedImageGray.Dilate(1);

             int morphOpSize = 89;
             Emgu.CV.StructuringElementEx element = new Emgu.CV.StructuringElementEx(morphOpSize, morphOpSize, morphOpSize / 2, morphOpSize / 2, Emgu.CV.CvEnum.CV_ELEMENT_SHAPE.CV_SHAPE_ELLIPSE);

             Emgu.CV.Image<Gray, byte> filtered = observedImageGray.MorphologyEx(element, Emgu.CV.CvEnum.CV_MORPH_OP.CV_MOP_TOPHAT, 1);
             //filtered = filtered.SmoothGaussian(3);



             filtered = filtered.ThresholdAdaptive(new Gray(255), Emgu.CV.CvEnum.ADAPTIVE_THRESHOLD_TYPE.CV_ADAPTIVE_THRESH_GAUSSIAN_C, Emgu.CV.CvEnum.THRESH.CV_THRESH_BINARY, 15, new Gray(-7));
             //result = combine(filtered); return result;
             //Emgu.CV.Contour<System.Drawing.Point> c = filtered.FindContours();
             Emgu.CV.Contour<System.Drawing.Point> contour =  filtered.FindContours(Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE, Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_CCOMP);
             //var c = filtered.FindContours( .FindContours(Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE, Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_CCOMP);
            



            List<Ellipse> ellipses = new List<Ellipse>();
            
            while (contour != null)
            {
                if (contour.Count() >= _min_contour_count)
                {
                    System.Drawing.PointF[] mypoints = Array.ConvertAll(
                      contour.ToArray<System.Drawing.Point>(),
                      value => new System.Drawing.PointF(value.X, value.Y)
                    );

                    Ellipse e = Emgu.CV.PointCollection.EllipseLeastSquareFitting(mypoints);
                    MCvBox2D box = e.MCvBox2D;
                    box.size.Height *= 0.5f;
                    box.size.Width *= 0.5f;
                    Ellipse final_ellipse = new Ellipse(box);

                    DenseMatrix m = DenseMatrix.CreateDiagonal(3, 3, 1);
                    double a = final_ellipse.MCvBox2D.size.Width;
                    double b = final_ellipse.MCvBox2D.size.Height;
                    double s = a / b;
                    DenseMatrix scale = DenseMatrix.CreateDiagonal(3, 3, 1);
                    scale[0, 0] = s;
                    double angle = final_ellipse.MCvBox2D.angle / 180 * Math.PI;
                    m[0, 0] = Math.Cos(angle);
                    m[0, 1] = -Math.Sin(angle);
                    m[0, 2] = final_ellipse.MCvBox2D.center.X;
                    m[1, 0] = Math.Sin(angle);
                    m[1, 1] = Math.Cos(angle);
                    m[1, 2] = final_ellipse.MCvBox2D.center.Y;

                    m = scale * m;
                    MathNet.Numerics.LinearAlgebra.Matrix<double> inv = m.Inverse();
                    List<double> ratings = new List<double>();


                    double width = observedImage.Width;
                    double height = observedImage.Height;

                    foreach (System.Drawing.PointF p in mypoints)
                    {
                        DenseVector x = new DenseVector(new double[] { p.X, p.Y, 1 });
                        MathNet.Numerics.LinearAlgebra.Matrix<double> invX = (inv.Multiply(x.ToColumnMatrix()));

                        IEnumerable<Tuple<int, MathNet.Numerics.LinearAlgebra.Vector<double>>> columnsInvX = invX.EnumerateColumnsIndexed(0, 1);
                        MathNet.Numerics.LinearAlgebra.Vector<double> r = columnsInvX.ElementAt(0).Item2;
                        
                        r = r.Normalize(2);
                        MathNet.Numerics.LinearAlgebra.Vector<Double> closest = r * a;

                        MathNet.Numerics.LinearAlgebra.Matrix<double> mClosest = (m.Multiply(closest.ToColumnMatrix()));
                        IEnumerable<Tuple<int, MathNet.Numerics.LinearAlgebra.Vector<double>>> columnsMClosest = mClosest.EnumerateColumnsIndexed(0, 1);
                        MathNet.Numerics.LinearAlgebra.Vector<Double> closest_in_world = columnsMClosest.ElementAt(0).Item2;


                        System.Drawing.Point mypoint = new System.Drawing.Point((int)closest_in_world[0], (int)closest_in_world[1]);
                        MathNet.Numerics.LinearAlgebra.Vector<Double> distance_in_world = closest_in_world - x;
 
                        ratings.Add(distance_in_world.Norm(2));
                    }

                    ratings.Sort();
                    double rating;
                    if (ratings.Count % 2 == 0)
                    {
                        rating = (ratings[ratings.Count / 2] + ratings[ratings.Count / 2 + 1]) * 0.5;
                    }
                    else
                    {
                        rating = ratings[(ratings.Count + 1) / 2];
                    }
                    if (rating < _distance_threshold)
                    {
                        ellipses.Add(final_ellipse);
                    }


                }
                contour = contour.HNext;
            }

            ellipses.Sort(
              (a, b) =>
              {
                  double dista = a.MCvBox2D.center.X * a.MCvBox2D.center.X + a.MCvBox2D.center.Y * a.MCvBox2D.center.Y;
                  double distb = b.MCvBox2D.center.X * b.MCvBox2D.center.X + b.MCvBox2D.center.Y * b.MCvBox2D.center.Y;
                  return dista.CompareTo(distb);
              }
            );


            if (IsDiagnoseMode)
            {
                diagnoseModeResultImg = observedImage.Clone();
                Bgr bgr = ThomsonReutersBrandColor;
                MCvFont f = new MCvFont(Emgu.CV.CvEnum.FONT.CV_FONT_HERSHEY_PLAIN, 0.8, 0.8);
                int count = 1;
                foreach (Ellipse e in ellipses)
                {
                    diagnoseModeResultImg.Draw(e, bgr, 2);
                    diagnoseModeResultImg.Draw(count.ToString(), ref f, new System.Drawing.Point((int)e.MCvBox2D.center.X, (int)e.MCvBox2D.center.Y), bgr);
                    count++;
                }

                MCvFont font = new MCvFont(Emgu.CV.CvEnum.FONT.CV_FONT_HERSHEY_PLAIN, 5.0, 5.0);
                font.thickness = 3;
                diagnoseModeResultImg.Draw("Hits: " + ellipses.Count.ToString(), ref font, new Point(100, 100), ThomsonReutersBrandColor);
            }

            return Math.Min((double) ellipses.Count / 10.0, 1.0);
        }



        private double DetectImage_FeatureDetectionOLD(Image<Bgr, byte> observedImage) 
        {
            double probability = 0;
            foreach (KeyValuePair<string, Image<Bgr, byte>> modelImg in positiveModels)
            {
                double prob = ImagesMatch_FeatureDetectionOLD(modelImg.Value, observedImage);
                probability = Math.Max(probability, prob);
            }
            return probability;
        }


        private double ImagesMatch_FeatureDetectionOLD(Image<Bgr, byte> modelImage, Image<Bgr, byte> observedImage)
        {
            double result = 0;
            try
            {
                int k = 2;
                double uniquenessThreshold = 0.45; // 0.45
                FastDetector fastCPU = new FastDetector(10, true);
                Freak descriptor = new Freak(true, true, 22.0F, 4);

                VectorOfKeyPoint modelKeyPoints;
                VectorOfKeyPoint observedKeyPoints;
                Matrix<int> indices;
                Matrix<byte> mask;
                HomographyMatrix homography = null;

                /*float[,] loGKernel = {
                                      {0,-1,0},
                                      {-1, 4, -1},
                                      {0, -1, 0}
                                     };*/
                  
                // Apply Laplacian of a Gaussian (LoG) convolution kernel

                FastDetector fastModelCPU = new FastDetector(10, true);
                Image<Gray, Byte> modelImageGray = modelImage.Convert<Gray, byte>().PyrDown().PyrUp();  //Convert model image to grayscale
                modelImageGray._EqualizeHist();
                modelImageGray = modelImageGray.Convolution(new ConvolutionKernelF(loGKernelSigma14)).Convert<Gray, Byte>();
                
                //modelImageGray = modelImageGray.Dilate(1).Erode(1);

                //extract features from the object image
                modelKeyPoints = fastModelCPU.DetectKeyPointsRaw(modelImageGray, null);
                Matrix<Byte> modelDescriptors = descriptor.ComputeDescriptorsRaw(modelImageGray, null, modelKeyPoints);
                //modelKeyPoints.FilterByKeypointSize(5, 80); // FIXME ?

                Image<Gray, Byte> observedImageGray = observedImage.Convert<Gray, Byte>().PyrDown().PyrUp(); //Convert observed image to grayscale
                observedImageGray._EqualizeHist();
                //observedImageGray = observedImageGray.SmoothBlur(7, 7);
                // Gaussian Smoothing
                observedImageGray = observedImageGray.Convolution(new ConvolutionKernelF(loGKernelSigma14)).Convert<Gray, Byte>();
                observedImageGray = observedImageGray.SmoothMedian(7);

                // extract features from the observed image
                observedKeyPoints = fastCPU.DetectKeyPointsRaw(observedImageGray, null);
                //observedKeyPoints.FilterByKeypointSize(7, 80); // FIXME ??
                Matrix<Byte> observedDescriptors = descriptor.ComputeDescriptorsRaw(observedImageGray, null, observedKeyPoints);

                BruteForceMatcher<byte> matcher = new BruteForceMatcher<byte>(DistanceType.L2Sqr);
                matcher.Add(modelDescriptors);

                indices = new Matrix<int>(observedDescriptors.Rows, k);
                using (Matrix<float> dist = new Matrix<float>(observedDescriptors.Rows, k))
                {
                    matcher.KnnMatch(observedDescriptors, indices, dist, k, null);
                    mask = new Matrix<byte>(dist.Rows, 1);
                    mask.SetValue(255);
                    Features2DToolbox.VoteForUniqueness(dist, uniquenessThreshold, mask);
                }

                int nonZeroCount = CvInvoke.cvCountNonZero(mask);
                if (nonZeroCount >= 10)
                {
                    nonZeroCount = Features2DToolbox.VoteForSizeAndOrientation(modelKeyPoints, observedKeyPoints, indices, mask, 10, 20);
                    //if (nonZeroCount >= 20)
                    //    homography = Features2DToolbox.GetHomographyMatrixFromMatchedFeatures(modelKeyPoints, observedKeyPoints, indices, mask, 2);
                }
                this.matchedFeatures = nonZeroCount;

                if (IsDiagnoseMode)
                {
                    createDiagnoseInformation(modelImageGray, observedImageGray, homography, modelKeyPoints, observedKeyPoints, indices, mask);
                }

                //if (homography != null)
                //{
                //    result = 1.0; // Probability is almost 1 if homography is actually found
                //}
                //else
                //{
                if (nonZeroCount >= 10)
                {
                    result = 0.8;
                }
                else
                {
                    result = 0;
                }
                //}
            }
            catch (Exception e)
            { }
            return result;
        }
        
        private double ImagesMatch_FeatureDetectionOLD2(Image<Bgr, byte> modelImage, Image<Bgr, byte> observedImage)
        {
            double result = 0;
            try
            {
                int k = 2;
                double uniquenessThreshold = 0.5; // 0.45
                FastDetector fastCPU = new FastDetector(10, true);
                Freak descriptor = new Freak(true, true, 22.0F, 4);

                VectorOfKeyPoint modelKeyPoints;
                VectorOfKeyPoint observedKeyPoints;
                Matrix<int> indices;
                Matrix<byte> mask;
                HomographyMatrix homography = null;


                FastDetector fastModelCPU = new FastDetector(5, true);
                Image<Gray, Byte> modelImageGray = modelImage.Convert<Gray, byte>().PyrDown().PyrUp().Canny(50, 50);  //Convert model image to grayscale
                modelImageGray._EqualizeHist();
                //modelImageGray = modelImageGray.Dilate(1).Erode(1);

                //extract features from the object image
                modelKeyPoints = fastModelCPU.DetectKeyPointsRaw(modelImageGray, null);
                Matrix<Byte> modelDescriptors = descriptor.ComputeDescriptorsRaw(modelImageGray, null, modelKeyPoints);
                //modelKeyPoints.FilterByKeypointSize(5, 80); // FIXME ?

                Image<Gray, Byte> observedImageGray = observedImage.Convert<Gray, Byte>().PyrDown().PyrUp().Canny(50, 50); //Convert observed image to grayscale
                observedImageGray._EqualizeHist();
                //observedImageGray = observedImageGray.SmoothBlur(7, 7);
                // Gaussian Smoothing
                observedImageGray = observedImageGray.SmoothGaussian(3);

                // extract features from the observed image
                observedKeyPoints = fastCPU.DetectKeyPointsRaw(observedImageGray, null);
                //observedKeyPoints.FilterByKeypointSize(5, 80); // FIXME ??
                Matrix<Byte> observedDescriptors = descriptor.ComputeDescriptorsRaw(observedImageGray, null, observedKeyPoints);
                BruteForceMatcher<byte> matcher = new BruteForceMatcher<byte>(DistanceType.L2Sqr);
                matcher.Add(modelDescriptors);

                indices = new Matrix<int>(observedDescriptors.Rows, k);
                using (Matrix<float> dist = new Matrix<float>(observedDescriptors.Rows, k))
                {
                    matcher.KnnMatch(observedDescriptors, indices, dist, k, null);
                    mask = new Matrix<byte>(dist.Rows, 1);
                    mask.SetValue(255);
                    Features2DToolbox.VoteForUniqueness(dist, uniquenessThreshold, mask);
                }

                int nonZeroCount = CvInvoke.cvCountNonZero(mask);
                if (nonZeroCount >= 20)
                {
                    nonZeroCount = Features2DToolbox.VoteForSizeAndOrientation(modelKeyPoints, observedKeyPoints, indices, mask, 1.1, 20);
                    //if (nonZeroCount >= 20)
                    //    homography = Features2DToolbox.GetHomographyMatrixFromMatchedFeatures(modelKeyPoints, observedKeyPoints, indices, mask, 2);
                }
                this.matchedFeatures = nonZeroCount;

                if (IsDiagnoseMode)
                {
                    createDiagnoseInformation(modelImageGray, observedImageGray, homography, modelKeyPoints, observedKeyPoints, indices, mask);
                }

                //if (homography != null)
                //{
                //    result = 1.0; // Probability is almost 1 if homography is actually found
                //}
                //else
                //{
                if (nonZeroCount > 20)
                {
                    result = 0.8;
                }
                else
                {
                    result = 0;
                }
                //}
            }
            catch (Exception e)
            { }
            return result;
        }

        public void PrepareFeatureDetection()
        {
            imgCollectionMatcher = new ImageCollectionMatcher();
            positiveModelsInfo = new Dictionary<string, ImageInfo>();

            foreach (KeyValuePair<string, Image<Bgr, Byte>> model in positiveModels)
            {
                Image<Gray, Byte> modelImageGray = model.Value.Convert<Gray, byte>().PyrDown().PyrUp();  //Convert model image to grayscale
                modelImageGray._EqualizeHist();
                //modelImageGray = modelImageGray.Convolution(new ConvolutionKernelF(loGKernelSigma14)).Convert<Gray, Byte>();

                //extract features from the object image
                FastDetector fastModelCPU = new FastDetector(10, true);
                VectorOfKeyPoint modelKeyPoints = fastModelCPU.DetectKeyPointsRaw(modelImageGray, null);

                Freak descriptor = new Freak(true, true, 22.0F, 4);
                Matrix<byte> modelDescriptors = descriptor.ComputeDescriptorsRaw(modelImageGray, null, modelKeyPoints);

                // Store model image information for later use
                positiveModelsInfo.Add(model.Key, new ImageInfo(modelImageGray, modelKeyPoints, modelDescriptors));

                Matrix<float> modelDescriptorsFloat = MatrixByteToFloat(modelDescriptors);
                imgCollectionMatcher.AddModelImage(model.Key, null, modelDescriptorsFloat);        
            }
        }


        private double DetectImageBasedOnFeatureDetection(Image<Bgr, byte> observedImage)
        {
            #region obtain obsImage keypoints

            FastDetector fastCPU = new FastDetector(10, true);
            Image<Gray, Byte> observedImageGray = observedImage.Convert<Gray, Byte>().PyrDown().PyrUp(); //Convert observed image to grayscale
            observedImageGray._EqualizeHist();
            //observedImageGray = observedImageGray.Convolution(new ConvolutionKernelF(loGKernelSigma14)).Convert<Gray, Byte>();
            observedImageGray = observedImageGray.SmoothMedian(7);


            // extract features from the observed image
            VectorOfKeyPoint observedKeyPoints;
            observedKeyPoints = fastCPU.DetectKeyPointsRaw(observedImageGray, null);
            #endregion

            #region obtain obsImage descriptors
            Freak descriptor = new Freak(true, true, 22.0F, 4);
            Matrix<byte> observedDescriptors = descriptor.ComputeDescriptorsRaw(observedImageGray, null, observedKeyPoints);
            observedKeyPoints.FilterByKeypointSize(7, 80);
            Matrix<float> observedDescriptorsFloat = MatrixByteToFloat(observedDescriptors);
            #endregion

            ImageKeyPointsAndDescriptors observedImageKeyPointsAndDescriptors = new ImageKeyPointsAndDescriptors(null, observedKeyPoints, observedDescriptorsFloat);
            List<IndecesMapping> matchResults = imgCollectionMatcher.Match(observedImageKeyPointsAndDescriptors);


            if (IsDiagnoseMode)
            {
                #region Create diagnose information
                IndecesMapping bestMatching = matchResults[0];
                this.matchedFeatures = bestMatching.Similarity;
                // get GrayScaleModelImage
                ImageInfo imgInfo = positiveModelsInfo[bestMatching.fileName];
                Image<Gray, byte> modelImgGray = imgInfo.ImgGray;
                VectorOfKeyPoint modelKeyPoints = imgInfo.Keypoints;
                Matrix<byte> modelDescriptors = imgInfo.Descriptors;

                // Computing the mask and index from imgCollectionMatcher is complex, so repeat matching just for displaying purposes
                BruteForceMatcher<byte> matcher = new BruteForceMatcher<byte>(DistanceType.L2Sqr);
                matcher.Add(modelDescriptors);
                Matrix<int> indices = new Matrix<int>(observedDescriptors.Rows, 2);
                Matrix<byte> mask;
                using (Matrix<float> dist = new Matrix<float>(observedDescriptors.Rows, 2))
                {
                    matcher.KnnMatch(observedDescriptors, indices, dist, 2, null);
                    mask = new Matrix<byte>(dist.Rows, 1);
                    mask.SetValue(255);
                    Features2DToolbox.VoteForUniqueness(dist, 0.6, mask);
                }

                int nonZeroCount = CvInvoke.cvCountNonZero(mask);
                if (nonZeroCount >= 10)
                {
                    nonZeroCount = Features2DToolbox.VoteForSizeAndOrientation(modelKeyPoints, observedKeyPoints, indices, mask, 10, 20);
                }

                diagnoseModeResultImg = Features2DToolbox.DrawMatches( modelImgGray, modelKeyPoints, observedImageGray, observedKeyPoints,
                    indices, new Bgr(255, 255, 255), new Bgr(255, 255, 255), mask, Features2DToolbox.KeypointDrawType.NOT_DRAW_SINGLE_POINTS);

                #endregion
            }
            // FIXME return consistent result
            if (IsDiagnoseMode)
            {
                //createDiagnoseInformation(modelImageGray, observedImageGray, homography, modelKeyPoints, observedKeyPoints, indices, mask);
            }



            return Math.Min( (double)matchResults[0].NormalizedSimilarity, 1.0); // FIXME
        }

        /// <summary>
        /// FIXME! probably too slow ??
        /// </summary>
        /// <param name="observedDescriptors"></param>
        /// <returns></returns>
        private static Matrix<float> MatrixByteToFloat(Matrix<byte> observedDescriptors)
        {
            byte[,] descByteData = observedDescriptors.Data;
            int rows = descByteData.GetLength(0);
            int cols = descByteData.GetLength(1);
            float[,] descFloatData = new float[rows, cols];
            for (int i = 0; i < rows; i++)
                for (int j = 0; j < cols; j++)
                    descFloatData[i, j] = (float)descByteData[i, j];
            return new Matrix<float>(descFloatData);

        }


        /// <summary>
        /// Helper function only, does not have impact on detection nor computation
        /// </summary>
        /// <param name="modelImage"></param>
        /// <param name="observedImage"></param>
        /// <param name="homography"></param>
        /// <param name="modelKeyPoints"></param>
        /// <param name="observedKeyPoints"></param>
        /// <param name="indices"></param>
        /// <param name="mask"></param>
         private void createDiagnoseInformation(Image<Gray, Byte> modelImage, Image<Gray, byte> observedImage, 
            HomographyMatrix homography, VectorOfKeyPoint modelKeyPoints, VectorOfKeyPoint observedKeyPoints, 
            Matrix<int> indices, Matrix<byte> mask)
        {

            //Draw the matched keypoints
            diagnoseModeResultImg = Features2DToolbox.DrawMatches(modelImage, modelKeyPoints, observedImage, observedKeyPoints,
               indices, new Bgr(255, 255, 255), new Bgr(255, 255, 255), mask, Features2DToolbox.KeypointDrawType.NOT_DRAW_SINGLE_POINTS);

            #region draw the projected region on the image
            if (homography != null)
            {  //draw a rectangle along the projected model
                Rectangle rect = modelImage.ROI;
                PointF[] pts = new PointF[] { 
               new PointF(rect.Left, rect.Bottom),
               new PointF(rect.Right, rect.Bottom),
               new PointF(rect.Right, rect.Top),
               new PointF(rect.Left, rect.Top)};
                homography.ProjectPoints(pts);

                diagnoseModeResultImg.DrawPolyline(Array.ConvertAll<PointF, Point>(pts, Point.Round), true, new Bgr(Color.Red), 5);
            }
            #endregion
        }

         public double ImageIsSpottedCat(Image<Bgr, byte> observedImage, bool singleImg) 
         {
             return ImageIsSpottedCat(observedImage, null, singleImg);
         }


        private Image<Bgr, byte> ScaleIfNeeded(Image<Bgr, byte> image)
        {
            Image<Bgr, byte> result = image;
            if(image.Width > 0) 
            {
                const double WR = 600.0;

                if (Math.Abs(image.Width - WR) > 100) 
                {
                    double scaleW = (double) WR / image.Width;
                    result = image.Resize(scaleW, INTER.CV_INTER_LINEAR);
                } 
            }

            return result;
        }

        // returns a number from 0 to 1 indicating confidence level
        public double ImageIsSpottedCat(Image<Bgr, byte> observedImage, Image<Bgr, float> observedImageFloat, bool singleImg)
        {
            // Preserve aspect ratio but change size to W=600, H=421 whenever possible 
            // Although most detectors are scale invariant (all 'training' has been done using this scale)
            // So, it is better to be conservative...
            observedImage = ScaleIfNeeded(observedImage);

            double[] confLevelsSingleImg = new double[] {0.8, 0.7, 0.9};
            double[] confLevelsImgCollection = new double[] {0.85, 0.4, 0.4};

            double[] confLevels = singleImg ? confLevelsSingleImg : confLevelsImgCollection;

            // Cascade filter (run detectors in known sequence, in order to maximize Accuracy)
            double probability = 0;


            if (singleImg)
            {
                probability = DetectImageLoG(observedImage);// DetectImage_FeatureDetection(observedImage); // DetectImage_FeatureDetection(observedImage);
                if (probability >= confLevels[0])
                {
                    return probability; // LoG (Laplacian of Gaussian) detector has tipically a very low FPR, so it is safe to say that if detected a cat, it is a cat
                    // FIXME (regarding DetectImageLoG need to tidy it up a bit, 
                    // so to include contrast validation between spot centers and points in the bisector of the segment between closest neighbors centers
                }
                else
                {
                    probability = DetectImage_FeatureDetectionOLD(observedImage);//DetectImageBasedOnFeatureDetection(observedImage);
                }
                /*    if (probability > confLevels[1])
                    {
                        probability = DetectImageLoG2(observedImage);
                        if (probability > confLevels[2])
                        {
                            return probability; // LoG2 (Laplacian of Gaussian higher 'apperture') this detector has tipically a very low FPR (a bit higher than LoG), so it is still safe to say that if detected a cat, it is a cat
                        }
                    }
                    else
                    {
                        // Throw more complex detectors into the game... :))
                        // Morphology Top Hat is excellent in detecting real spot borders, but is sensitive to scale variations and noise
                        // FIXME 
                        //probability = DetectImageHoughTransform(observedImage);
                        //if (probability > 0.9) 
                        {
                            return probability;
                        }
                    }//*
                
               
                //probability = DetectImage_FeatureDetectionOLD(observedImage);
                
                probability = 0.8; //test this idea !!*/
            }
            else
            {
                probability = DetectImageLoG2(observedImage);
                if (probability >= confLevels[0])
                {
                    return probability;
                }
                else
                {
                    probability = DetectImagTopHatMorphologyGoodDetector(observedImage);
                }
            }
            
            return probability;
        }
    }
}
