﻿namespace SpottedCatChallenge
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            System.Windows.Forms.ListViewGroup listViewGroup1 = new System.Windows.Forms.ListViewGroup("Spotted Cats", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup2 = new System.Windows.Forms.ListViewGroup("Others", System.Windows.Forms.HorizontalAlignment.Left);
            this.imageBox1 = new Emgu.CV.UI.ImageBox();
            this.selectInputDirBtn = new System.Windows.Forms.Button();
            this.dirNameTxtBox = new System.Windows.Forms.TextBox();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.button1 = new System.Windows.Forms.Button();
            this.outputDirTxtBox = new System.Windows.Forms.TextBox();
            this.lblInputFolder = new System.Windows.Forms.Label();
            this.lblOutputFOlder = new System.Windows.Forms.Label();
            this.changeOutputDirBtn = new System.Windows.Forms.Button();
            this.startStopDectectionBtn = new System.Windows.Forms.Button();
            this.folderBrowserDialog2 = new System.Windows.Forms.FolderBrowserDialog();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblMessages = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBoxResults = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnClearResults = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.txtScanned = new System.Windows.Forms.TextBox();
            this.saveResultsBtn = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblNEG = new System.Windows.Forms.Label();
            this.lblPOS = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblPositives = new System.Windows.Forms.Label();
            this.debugModeCheckbox = new System.Windows.Forms.CheckBox();
            this.labelInst1 = new System.Windows.Forms.Label();
            this.labelInst2 = new System.Windows.Forms.Label();
            this.labelInst3 = new System.Windows.Forms.Label();
            this.btnZoom = new System.Windows.Forms.Button();
            this.btnDetectorSettings = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.listViewImages = new SpottedCatChallenge.ListViewNF();
            this.colImage = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colProbability = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colIsSpottedCat = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ((System.ComponentModel.ISupportInitialize)(this.imageBox1)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.groupBoxResults.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageBox1
            // 
            this.imageBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.imageBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.imageBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("imageBox1.BackgroundImage")));
            this.imageBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.imageBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.imageBox1.FunctionalMode = Emgu.CV.UI.ImageBox.FunctionalModeOption.Minimum;
            this.imageBox1.InitialImage = null;
            this.imageBox1.Location = new System.Drawing.Point(594, 132);
            this.imageBox1.Name = "imageBox1";
            this.imageBox1.Size = new System.Drawing.Size(634, 508);
            this.imageBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imageBox1.TabIndex = 5;
            this.imageBox1.TabStop = false;
            this.imageBox1.Click += new System.EventHandler(this.imageBox1_Click);
            // 
            // selectInputDirBtn
            // 
            this.selectInputDirBtn.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectInputDirBtn.Location = new System.Drawing.Point(691, 16);
            this.selectInputDirBtn.Name = "selectInputDirBtn";
            this.selectInputDirBtn.Size = new System.Drawing.Size(171, 31);
            this.selectInputDirBtn.TabIndex = 0;
            this.selectInputDirBtn.Text = "Select &input folder...";
            this.selectInputDirBtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.selectInputDirBtn.UseVisualStyleBackColor = true;
            this.selectInputDirBtn.Click += new System.EventHandler(this.verifyDirBtn_Click);
            // 
            // dirNameTxtBox
            // 
            this.dirNameTxtBox.AllowDrop = true;
            this.dirNameTxtBox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dirNameTxtBox.Location = new System.Drawing.Point(116, 19);
            this.dirNameTxtBox.Name = "dirNameTxtBox";
            this.dirNameTxtBox.Size = new System.Drawing.Size(569, 26);
            this.dirNameTxtBox.TabIndex = 7;
            this.dirNameTxtBox.TabStop = false;
            this.dirNameTxtBox.TextChanged += new System.EventHandler(this.dirNameTxtBox_TextChanged);
            this.dirNameTxtBox.DragDrop += new System.Windows.Forms.DragEventHandler(this.dirNameTxtBox_DragDrop);
            this.dirNameTxtBox.DragEnter += new System.Windows.Forms.DragEventHandler(this.dirNameTxtBox_DragEnter);
            // 
            // folderBrowserDialog1
            // 
            this.folderBrowserDialog1.SelectedPath = "C:\\SpottedCat\\Images\\Test";
            // 
            // progressBar
            // 
            this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar.Location = new System.Drawing.Point(8, 654);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(1220, 19);
            this.progressBar.Step = 1;
            this.progressBar.TabIndex = 8;
            // 
            // button1
            // 
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Location = new System.Drawing.Point(111, 95);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(116, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "GetListYES";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // outputDirTxtBox
            // 
            this.outputDirTxtBox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.outputDirTxtBox.Location = new System.Drawing.Point(116, 55);
            this.outputDirTxtBox.Name = "outputDirTxtBox";
            this.outputDirTxtBox.ReadOnly = true;
            this.outputDirTxtBox.Size = new System.Drawing.Size(569, 26);
            this.outputDirTxtBox.TabIndex = 11;
            this.outputDirTxtBox.TabStop = false;
            // 
            // lblInputFolder
            // 
            this.lblInputFolder.AutoSize = true;
            this.lblInputFolder.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInputFolder.Location = new System.Drawing.Point(14, 22);
            this.lblInputFolder.Name = "lblInputFolder";
            this.lblInputFolder.Size = new System.Drawing.Size(83, 18);
            this.lblInputFolder.TabIndex = 12;
            this.lblInputFolder.Text = "Input folder";
            // 
            // lblOutputFOlder
            // 
            this.lblOutputFOlder.AutoSize = true;
            this.lblOutputFOlder.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOutputFOlder.Location = new System.Drawing.Point(14, 58);
            this.lblOutputFOlder.Name = "lblOutputFOlder";
            this.lblOutputFOlder.Size = new System.Drawing.Size(96, 18);
            this.lblOutputFOlder.TabIndex = 13;
            this.lblOutputFOlder.Text = "Output folder";
            // 
            // changeOutputDirBtn
            // 
            this.changeOutputDirBtn.Enabled = false;
            this.changeOutputDirBtn.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.changeOutputDirBtn.Location = new System.Drawing.Point(691, 52);
            this.changeOutputDirBtn.Name = "changeOutputDirBtn";
            this.changeOutputDirBtn.Size = new System.Drawing.Size(171, 31);
            this.changeOutputDirBtn.TabIndex = 1;
            this.changeOutputDirBtn.Text = "&Change output folder...";
            this.changeOutputDirBtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.changeOutputDirBtn.UseVisualStyleBackColor = true;
            this.changeOutputDirBtn.Click += new System.EventHandler(this.changeOutputDirBtn_Click);
            // 
            // startStopDectectionBtn
            // 
            this.startStopDectectionBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.startStopDectectionBtn.Enabled = false;
            this.startStopDectectionBtn.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startStopDectectionBtn.Location = new System.Drawing.Point(691, 89);
            this.startStopDectectionBtn.Name = "startStopDectectionBtn";
            this.startStopDectectionBtn.Size = new System.Drawing.Size(171, 31);
            this.startStopDectectionBtn.TabIndex = 2;
            this.startStopDectectionBtn.Tag = "1";
            this.startStopDectectionBtn.Text = "St&art detection";
            this.startStopDectectionBtn.UseVisualStyleBackColor = false;
            this.startStopDectectionBtn.Click += new System.EventHandler(this.startDectectionBtn_Click);
            // 
            // folderBrowserDialog2
            // 
            this.folderBrowserDialog2.SelectedPath = "C:\\SpottedCat\\Images\\Test";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblMessages});
            this.statusStrip1.Location = new System.Drawing.Point(0, 676);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1240, 22);
            this.statusStrip1.TabIndex = 20;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lblMessages
            // 
            this.lblMessages.Name = "lblMessages";
            this.lblMessages.Size = new System.Drawing.Size(0, 17);
            // 
            // groupBoxResults
            // 
            this.groupBoxResults.Controls.Add(this.panel1);
            this.groupBoxResults.Location = new System.Drawing.Point(879, 16);
            this.groupBoxResults.Name = "groupBoxResults";
            this.groupBoxResults.Size = new System.Drawing.Size(349, 110);
            this.groupBoxResults.TabIndex = 21;
            this.groupBoxResults.TabStop = false;
            this.groupBoxResults.Text = "Results";
            this.groupBoxResults.Visible = false;
            this.groupBoxResults.Enter += new System.EventHandler(this.groupBoxResults_Enter);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnClearResults);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txtTotal);
            this.panel1.Controls.Add(this.txtScanned);
            this.panel1.Controls.Add(this.saveResultsBtn);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.lblNEG);
            this.panel1.Controls.Add(this.lblPOS);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.lblPositives);
            this.panel1.Location = new System.Drawing.Point(6, 19);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(337, 84);
            this.panel1.TabIndex = 18;
            // 
            // btnClearResults
            // 
            this.btnClearResults.Enabled = false;
            this.btnClearResults.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClearResults.Location = new System.Drawing.Point(230, 30);
            this.btnClearResults.Name = "btnClearResults";
            this.btnClearResults.Size = new System.Drawing.Size(107, 25);
            this.btnClearResults.TabIndex = 30;
            this.btnClearResults.Text = "C&lear results";
            this.btnClearResults.UseVisualStyleBackColor = true;
            this.btnClearResults.Click += new System.EventHandler(this.btnClearResults_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(235, 4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(12, 18);
            this.label5.TabIndex = 29;
            this.label5.Text = "/";
            // 
            // txtTotal
            // 
            this.txtTotal.BackColor = System.Drawing.SystemColors.Control;
            this.txtTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotal.Location = new System.Drawing.Point(250, 3);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.ReadOnly = true;
            this.txtTotal.Size = new System.Drawing.Size(79, 20);
            this.txtTotal.TabIndex = 28;
            // 
            // txtScanned
            // 
            this.txtScanned.BackColor = System.Drawing.SystemColors.Control;
            this.txtScanned.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtScanned.Location = new System.Drawing.Point(153, 3);
            this.txtScanned.Name = "txtScanned";
            this.txtScanned.ReadOnly = true;
            this.txtScanned.Size = new System.Drawing.Size(79, 20);
            this.txtScanned.TabIndex = 27;
            // 
            // saveResultsBtn
            // 
            this.saveResultsBtn.Enabled = false;
            this.saveResultsBtn.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveResultsBtn.Location = new System.Drawing.Point(230, 54);
            this.saveResultsBtn.Name = "saveResultsBtn";
            this.saveResultsBtn.Size = new System.Drawing.Size(107, 25);
            this.saveResultsBtn.TabIndex = 23;
            this.saveResultsBtn.Text = "&Save results";
            this.saveResultsBtn.UseVisualStyleBackColor = true;
            this.saveResultsBtn.Click += new System.EventHandler(this.saveResultsBtn_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(131, 4);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(16, 18);
            this.label6.TabIndex = 26;
            this.label6.Text = ": ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(0, 4);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(127, 18);
            this.label7.TabIndex = 25;
            this.label7.Tag = "";
            this.label7.Text = "Scanned / Total #";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(78, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(16, 18);
            this.label4.TabIndex = 24;
            this.label4.Text = ": ";
            // 
            // lblNEG
            // 
            this.lblNEG.AutoEllipsis = true;
            this.lblNEG.AutoSize = true;
            this.lblNEG.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNEG.ForeColor = System.Drawing.Color.Red;
            this.lblNEG.Location = new System.Drawing.Point(102, 54);
            this.lblNEG.Name = "lblNEG";
            this.lblNEG.Size = new System.Drawing.Size(17, 18);
            this.lblNEG.TabIndex = 23;
            this.lblNEG.Tag = "";
            this.lblNEG.Text = "0";
            // 
            // lblPOS
            // 
            this.lblPOS.AutoSize = true;
            this.lblPOS.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPOS.ForeColor = System.Drawing.Color.Green;
            this.lblPOS.Location = new System.Drawing.Point(102, 30);
            this.lblPOS.Name = "lblPOS";
            this.lblPOS.Size = new System.Drawing.Size(17, 18);
            this.lblPOS.TabIndex = 21;
            this.lblPOS.Tag = "";
            this.lblPOS.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(78, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(16, 18);
            this.label3.TabIndex = 20;
            this.label3.Text = ": ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(0, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 18);
            this.label2.TabIndex = 19;
            this.label2.Tag = "";
            this.label2.Text = "Negatives";
            // 
            // lblPositives
            // 
            this.lblPositives.AutoSize = true;
            this.lblPositives.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPositives.ForeColor = System.Drawing.Color.Green;
            this.lblPositives.Location = new System.Drawing.Point(0, 30);
            this.lblPositives.Name = "lblPositives";
            this.lblPositives.Size = new System.Drawing.Size(72, 18);
            this.lblPositives.TabIndex = 17;
            this.lblPositives.Tag = "v";
            this.lblPositives.Text = "Positives";
            // 
            // debugModeCheckbox
            // 
            this.debugModeCheckbox.AutoSize = true;
            this.debugModeCheckbox.Location = new System.Drawing.Point(17, 103);
            this.debugModeCheckbox.Name = "debugModeCheckbox";
            this.debugModeCheckbox.Size = new System.Drawing.Size(88, 17);
            this.debugModeCheckbox.TabIndex = 22;
            this.debugModeCheckbox.Text = "Debug Mode";
            this.debugModeCheckbox.UseVisualStyleBackColor = true;
            this.debugModeCheckbox.Visible = false;
            // 
            // labelInst1
            // 
            this.labelInst1.AutoSize = true;
            this.labelInst1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInst1.Location = new System.Drawing.Point(835, 261);
            this.labelInst1.Name = "labelInst1";
            this.labelInst1.Size = new System.Drawing.Size(379, 18);
            this.labelInst1.TabIndex = 25;
            this.labelInst1.Text = "STEP 1. Select folder where input images are located";
            // 
            // labelInst2
            // 
            this.labelInst2.AutoSize = true;
            this.labelInst2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInst2.Location = new System.Drawing.Point(835, 309);
            this.labelInst2.Name = "labelInst2";
            this.labelInst2.Size = new System.Drawing.Size(307, 18);
            this.labelInst2.TabIndex = 26;
            this.labelInst2.Text = "STEP 2. (OPTIONAL) Change output folder";
            // 
            // labelInst3
            // 
            this.labelInst3.AutoSize = true;
            this.labelInst3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInst3.Location = new System.Drawing.Point(835, 353);
            this.labelInst3.Name = "labelInst3";
            this.labelInst3.Size = new System.Drawing.Size(232, 18);
            this.labelInst3.TabIndex = 27;
            this.labelInst3.Text = "STEP 3. Start detection process";
            // 
            // btnZoom
            // 
            this.btnZoom.BackColor = System.Drawing.Color.White;
            this.btnZoom.Enabled = false;
            this.btnZoom.Image = ((System.Drawing.Image)(resources.GetObject("btnZoom.Image")));
            this.btnZoom.Location = new System.Drawing.Point(594, 108);
            this.btnZoom.Name = "btnZoom";
            this.btnZoom.Size = new System.Drawing.Size(24, 23);
            this.btnZoom.TabIndex = 28;
            this.btnZoom.UseVisualStyleBackColor = false;
            this.btnZoom.Visible = false;
            this.btnZoom.Click += new System.EventHandler(this.btnZoom_Click);
            // 
            // btnDetectorSettings
            // 
            this.btnDetectorSettings.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDetectorSettings.Location = new System.Drawing.Point(514, 88);
            this.btnDetectorSettings.Name = "btnDetectorSettings";
            this.btnDetectorSettings.Size = new System.Drawing.Size(171, 31);
            this.btnDetectorSettings.TabIndex = 29;
            this.btnDetectorSettings.Text = "&Detector settings...";
            this.btnDetectorSettings.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDetectorSettings.UseVisualStyleBackColor = true;
            this.btnDetectorSettings.Click += new System.EventHandler(this.btnDetectorSettings_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1196, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 13);
            this.label1.TabIndex = 30;
            this.label1.Text = "v1.6";
            // 
            // listViewImages
            // 
            this.listViewImages.AllowColumnReorder = true;
            this.listViewImages.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.listViewImages.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colImage,
            this.colProbability,
            this.colIsSpottedCat});
            this.listViewImages.Cursor = System.Windows.Forms.Cursors.Default;
            this.listViewImages.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listViewImages.FullRowSelect = true;
            this.listViewImages.GridLines = true;
            listViewGroup1.Header = "Spotted Cats";
            listViewGroup1.Name = "lvGroupSpottedCats";
            listViewGroup2.Header = "Others";
            listViewGroup2.Name = "lvGroupOthers";
            this.listViewImages.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup1,
            listViewGroup2});
            this.listViewImages.Location = new System.Drawing.Point(15, 132);
            this.listViewImages.MultiSelect = false;
            this.listViewImages.Name = "listViewImages";
            this.listViewImages.Size = new System.Drawing.Size(561, 508);
            this.listViewImages.TabIndex = 15;
            this.listViewImages.UseCompatibleStateImageBehavior = false;
            this.listViewImages.View = System.Windows.Forms.View.Details;
            this.listViewImages.SelectedIndexChanged += new System.EventHandler(this.listViewImages_SelectedIndexChanged);
            this.listViewImages.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.listViewImages_KeyPress);
            // 
            // colImage
            // 
            this.colImage.Text = "Image";
            this.colImage.Width = 381;
            // 
            // colProbability
            // 
            this.colProbability.Text = "Probability";
            this.colProbability.Width = 100;
            // 
            // colIsSpottedCat
            // 
            this.colIsSpottedCat.Text = "IsSCat";
            this.colIsSpottedCat.Width = 65;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1240, 698);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnDetectorSettings);
            this.Controls.Add(this.btnZoom);
            this.Controls.Add(this.labelInst3);
            this.Controls.Add(this.labelInst2);
            this.Controls.Add(this.labelInst1);
            this.Controls.Add(this.debugModeCheckbox);
            this.Controls.Add(this.groupBoxResults);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.startStopDectectionBtn);
            this.Controls.Add(this.changeOutputDirBtn);
            this.Controls.Add(this.listViewImages);
            this.Controls.Add(this.lblOutputFOlder);
            this.Controls.Add(this.lblInputFolder);
            this.Controls.Add(this.outputDirTxtBox);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.dirNameTxtBox);
            this.Controls.Add(this.selectInputDirBtn);
            this.Controls.Add(this.imageBox1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thomson Reuters - Spotted Cat Challenge";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormTesting_Load);
            ((System.ComponentModel.ISupportInitialize)(this.imageBox1)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBoxResults.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Emgu.CV.UI.ImageBox imageBox1;
        private System.Windows.Forms.Button selectInputDirBtn;
        private System.Windows.Forms.TextBox dirNameTxtBox;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox outputDirTxtBox;
        private System.Windows.Forms.Label lblInputFolder;
        private System.Windows.Forms.Label lblOutputFOlder;
        private ListViewNF listViewImages;
        private System.Windows.Forms.ColumnHeader colImage;
        private System.Windows.Forms.ColumnHeader colProbability;
        private System.Windows.Forms.ColumnHeader colIsSpottedCat;
        private System.Windows.Forms.Button changeOutputDirBtn;
        private System.Windows.Forms.Button startStopDectectionBtn;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog2;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lblMessages;
        private System.Windows.Forms.GroupBox groupBoxResults;
        private System.Windows.Forms.Button saveResultsBtn;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblNEG;
        private System.Windows.Forms.Label lblPOS;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblPositives;
        private System.Windows.Forms.CheckBox debugModeCheckbox;
        private System.Windows.Forms.Label labelInst1;
        private System.Windows.Forms.Label labelInst2;
        private System.Windows.Forms.Label labelInst3;
        private System.Windows.Forms.Button btnZoom;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtTotal;
        private System.Windows.Forms.TextBox txtScanned;
        private System.Windows.Forms.Button btnDetectorSettings;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnClearResults;

    }
}