﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Features2D;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using Emgu.CV.Util;
using System.Diagnostics;
using System.IO;
using SpottedCatChallenge.MeasurementHelpers;
using System.Xml.Serialization;
using System.Resources;
using System.Threading;


namespace SpottedCatChallenge
{
    public partial class FormMain : Form
    {
        static string PATH_POSITIVE_IMAGES = "Cheetahs";
        static string PATH_NEGATIVE_IMAGES = "NonCheetahs";
        static string PATH_DEBUG_IMAGES = "DEBUG";

        #region More from training phase : solution verifier encapsulate the knowledge from a known set of input images, used for training
        private SolutionVerifier solutionVerifier;
        #endregion
        private string selectedDirectory;
        private string outputDirectory;
        private bool outputDirAlreadyExistsAndNotEmpty;
        private bool firstSpottedCat = true;
        private bool firstImageSelected = true;
        private int positives = 0;
        private int negatives = 0;
        private SpottedCatChallengeResults detectionResults = null;
        private bool isDebugMode = false;
        private List<string> errorList;
        private int countScannedImgs = 0;
        private bool detectorShouldContinue = true;
        private DetectionParameters currentDetectorParams;

        public bool DetectorShouldContinue
        {
            get { return detectorShouldContinue; }
            set { detectorShouldContinue = value; }
        }
        
        protected int Positives {
            get { return positives; }
            set { positives = value; lblPOS.Text = positives.ToString(); }
        }

        protected int Negatives
        {
            get { return negatives; }
            set { negatives = value; lblNEG.Text = negatives.ToString(); }
        }

        private ListViewGroup lvgIs;
        private ListViewGroup lvgIsNot;
        private Color alertColor = Color.FromArgb(249, 74, 74);
        private Color alertColorInputFolder = Color.FromArgb(242, 176, 157);
        private Color normalColor = Color.Black;

        protected string SelectedDirectory {
            get { return selectedDirectory; }
            set { selectedDirectory = value; this.dirNameTxtBox.Text = selectedDirectory; }
        }

        protected string OutputDirectory
        {
            get { return outputDirectory; }
            set { 
                    outputDirectory = value; 
                    this.outputDirTxtBox.Text = outputDirectory;
                    bool enableStartDetection = false;

                    if (currentDetectorParams.OverrideOutputFolder) 
                    {
                        outputDirAlreadyExistsAndNotEmpty = Directory.Exists(outputDirectory) && !DirectoryIsEmpty(outputDirectory);
                        if (outputDirAlreadyExistsAndNotEmpty)
                        {
                            if (MessageBox.Show("Delete directory " + outputDirectory + " ?", "Confirmation",
                                MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
                            {
                                Directory.Delete(outputDirectory, true);

                                Thread.Sleep(1000);
                                bool dirExists = true;
                                int iters = 0;
                                while (dirExists && iters < 20) 
                                {
                                    iters++;
                                    dirExists = Directory.Exists(outputDirectory);
                                    if(dirExists) Thread.Sleep(2000);
                                } 
                            }
                        }
                    }

                    outputDirAlreadyExistsAndNotEmpty = Directory.Exists(outputDirectory) && !DirectoryIsEmpty(outputDirectory);

                    if (outputDirAlreadyExistsAndNotEmpty)
                    {
                        outputDirTxtBox.BackColor = alertColor;
                        //changeOutputDirBtn.BackColor = alertColor;
                        MessageIndication("Output directory already exists and it is not empty, please select another OUTPUT directory...");
                    }
                    else
                    {
                        // Try to create output directory
                        try
                        {
                            if (!Directory.Exists(outputDirectory)) { Directory.CreateDirectory(outputDirectory); }
                            outputDirTxtBox.BackColor = SystemColors.Control;
                            MessageIndication("");
                            enableStartDetection = true;
                        }
                        catch (Exception e)
                        {
                            MessageIndication("Unable to create output directory, please validate if destination is write protected");
                        }
                    }

                    startStopDectectionBtn.Enabled = enableStartDetection;
            }
        }

        private void setDefaultDetectorParameters()
        {
            currentDetectorParams.IsDiagnoseMode = true; // default
            currentDetectorParams.Sensitivity = 40;
            currentDetectorParams.GenerateDebugImagesPOS = true;
            currentDetectorParams.GenerateDebugImagesNEG = true;
            currentDetectorParams.OverrideOutputFolder = false;
        }

        public FormMain()
        {
            InitializeComponent();

            currentDetectorParams = new DetectionParameters();
            setDefaultDetectorParameters();

            solutionVerifier = new SolutionVerifier(debugModeCheckbox.Checked, Path.GetDirectoryName(Application.ExecutablePath));

            this.listViewImages.ListViewItemSorter = new ListViewItemComparer();
            errorList = new List<string>();

            System.Windows.Forms.ToolTip ToolTip1 = new System.Windows.Forms.ToolTip();
            ToolTip1.SetToolTip(this.dirNameTxtBox, "Drag & Drop Windows folder containing input images");
            //RunNewTest();
        }


        void RunNewTest() 
        {
            string obsImg = @"C:\SpottedCat\Images\TEST\122673420110809440322558575265776895644_0.jpg";
            Image<Bgr, Byte> observedImageOrig = new Image<Bgr, byte>(obsImg); //"box_in_scene-new.png");

            EllipseDetection ellipDetection = new EllipseDetection();


            imageBox1.Image = ellipDetection.ProcessImage(observedImageOrig);
        }

        void RunTest1()
        {
            DetectionParameters detectorParams = new DetectionParameters();
            detectorParams.IsDiagnoseMode = true;
            //detectorParams.CheetahModels.Add(@"C:\Emgu\emgucv-windows-universal-cuda 2.9.0.1922\Emgu.CV.Example\SURFFeature\178836460428251366010935160763172824035_1.jpg");
            //detectorParams.CheetahModels.Add("cheetah2.png");
            //detectorParams.CheetahModels.Add(@"C:\SpottedCat\Images\ALL\SILUETA.jpg");
            //detectorParams.CheetahModels.Add(@"C:\SpottedCat\Images\ALL\SILUETA2.jpg");
            /*detectorParams.CheetahModels.Add(@"C:\SpottedCat\Images\FEATURES\feature1.png");
            detectorParams.CheetahModels.Add(@"C:\SpottedCat\Images\FEATURES\feature2.png");
            detectorParams.CheetahModels.Add(@"C:\SpottedCat\Images\FEATURES\feature3.png");
            */
            //detectorParams.CheetahModels.Add("cheetah1.png");
            detectorParams.PositiveModels.Clear();
            for (int k = 1; k <= 1; k++)
            {
                detectorParams.PositiveModels.Add(@"C:\SpottedCat\Images\FEATURES\feature" + k.ToString() + ".png");
            }
            //detectorParams.PositiveModels.Add(@"C:\SpottedCat\Images\FEATURES\feature0.png");


            SpottedCatDetector detector = new SpottedCatDetector(detectorParams);
            string obsImg = @"C:\SpottedCat\Images\ALL\100595386135642733909362945398068011795_0.jpg";  // NICE EXAMPLE TO SHOW: C:\SpottedCat\Images\Test\106001772098205737045303485026780216631_2.jpg
            //string obsImg = "sample2.jpg"; // cheetah cerca   
            //string obsImg = "box_in_scene.png";  // cajas
            //string obsImg = "10133052659369446788107695243109033091_0.jpg"; // hiena
            //string obsImg = "107210424230124423214414482195189135250_2.jpg"; // cebra
            //string obsImg = "101710936845483432573822999089226384775_2.jpg"; // paisaje
            //string obsImg = "99918492929935310734214655567859418444_2.jpg"; // cheetah yendose
            //string obsImg = "104795964304951838501616404708546057546_1.jpg"; //cheetah muy muy cerca
            //string obsImg = "sample1.jpg"; //cheetah muy cerca - matchea con box 3
            //string obsImg = "115872033954632641344316139114474918971_2.jpg"; // cheetah
            //string obsImg = "box_in_scene-new.png"; // cheetah
            //string obsImg = "105426507922298233889447262088403777181_0.jpg"; // pedacito de cheetah
            //string obsImg = "10244797732909875767514294212816901970_0.jpg"; // cheetah con flash de cerca
            Image<Bgr, Byte> observedImageOrig = new Image<Bgr, byte>(obsImg); //"box_in_scene-new.png");
            Image<Bgr, float> observedImageOrigFloat = new Image<Bgr, float>(obsImg); //"box_in_scene-new.png");
            detector.PrepareFeatureDetection();

            Stopwatch watch = Stopwatch.StartNew();
            double probability = detector.ImageIsSpottedCat(observedImageOrig, observedImageOrigFloat, true);

            string result = "RESULT: ";
            if (probability >= 0.8)
            {
                result += "YES";
            }
            else
            {
                result += "NO";
            }

            if (detector.IsDiagnoseMode)
            {
                imageBox1.Image = detector.DiagnoseModeResultImg;
            }
           lblMessages.Text = String.Format("Matched in {0} milliseconds. {1} matches, {2}", watch.ElapsedMilliseconds, detector.MatchedFeatures, result);
           
        }


        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void imageBox1_Click(object sender, EventArgs e)
        {

        }

        private void MessageIndication(string message)
        {
            errorList.Add(message);
            lblMessages.Text = message; // FIXME MULTILANG
        
        }

        /// <summary>
        /// Extracted from http://stackoverflow.com/questions/755574/how-to-quickly-check-if-folder-is-empty-net
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public bool DirectoryIsEmpty(string path)
        {
            int fileCount = Directory.GetFiles(path).Length;
            if (fileCount > 0)
            {
                return false;
            }

            string[] dirs = Directory.GetDirectories(path);
            foreach (string dir in dirs)
            {
                if (!DirectoryIsEmpty(dir))
                {
                    return false;
                }
            }

            return true;
        }

        private void SetOutputDirectory(string path)
        {
            string outputDir;
            if (path.EndsWith("\\") || path.EndsWith("/"))
            {
                outputDir = path.Substring(0, path.Length - 1);
            }
            else
            {
                outputDir = path;
            }
            outputDir += "-output\\";


            //string outputDir = Path.Combine(path, "output");
            /*if (debugModeCheckbox.Checked) {
                if (Directory.Exists(outputDir)) { try { Directory.Delete(outputDir, true); } catch (Exception) { } }
            }*/
            this.OutputDirectory = outputDir;

            changeOutputDirBtn.Enabled = true;
        }

        private void verifyDirBtn_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.SelectedDirectory = folderBrowserDialog1.SelectedPath;

                // Try to assign unused outputdir
                SetOutputDirectory(selectedDirectory);
            }
        }

        private void changeOutputDirBtn_Click(object sender, EventArgs e)
        {
            // This is a helper for the user to select a new output directory when already used
            // or to change a 'good' output directory previously selected (just for convenience)
            // if ouputDirExistsAndNotEmpty then start in input folder else display currently selected outputdir
            folderBrowserDialog2.SelectedPath = this.outputDirAlreadyExistsAndNotEmpty ? this.selectedDirectory : this.OutputDirectory;
            

            if (folderBrowserDialog2.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.OutputDirectory = folderBrowserDialog2.SelectedPath;
            }
        }

        private void startDectectionBtn_Click(object sender, EventArgs e)
        {            
            bool modeStart = ((string)startStopDectectionBtn.Tag).Equals("1");
            if (modeStart)
            {
                this.DetectorShouldContinue = true;
                startStopDectectionBtn.Tag = "0"; // set this immediately to Stop mode
                startDectection();
            }
            else
            {
                if (MessageBox.Show("Stop the detection currently running ?", "Confirmation",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
                {
                    stopDetection();
                }
            }
        }

        private void startStopButtonToStopMode() 
        {
            startStopDectectionBtn.Text = "St&op Detection";
            startStopDectectionBtn.BackColor = Color.FromArgb(255, 128, 128);
            startStopDectectionBtn.Enabled = true;
        }


        private void startStopButtonToStartMode()
        {
            startStopDectectionBtn.Text = "St&art Detection";
            startStopDectectionBtn.BackColor = Color.FromArgb(128, 255, 128);
            startStopDectectionBtn.Enabled = true;
            startStopDectectionBtn.Tag = "1"; // set this immediately to Stop mode
        }

        private void stopDetection() {
            this.DetectorShouldContinue = false;
            btnDetectorSettings.Enabled = true;
        }

        private void startDectection()
        {
            startStopDectectionBtn.Enabled = false;
            selectInputDirBtn.Enabled = false;
            changeOutputDirBtn.Enabled = false;
            saveResultsBtn.Enabled = false;
            btnClearResults.Enabled = false;
            groupBoxResults.Visible = true;
            btnDetectorSettings.Enabled = false;
            labelInst1.Visible = false;
            labelInst2.Visible = false;
            labelInst3.Visible = false;
            firstSpottedCat = true;
            firstImageSelected = true;
            btnZoom.Enabled = true;
            countScannedImgs = 0;
            this.Positives = 0;
            this.negatives = 0;
            txtScanned.Text = countScannedImgs.ToString();
            try
            {
                isDebugMode = this.debugModeCheckbox.Checked;
                DirectoryInfo di = new DirectoryInfo(selectedDirectory);
                var filters = new[] { ".bmp", ".jpg", "*.png", "*.gif", "*.tiff" }; // add more
                List<FileInfo> files = di.GetFiles("*.*", SearchOption.TopDirectoryOnly).Where(file => filters.Contains(file.Extension, StringComparer.OrdinalIgnoreCase)).ToList();
                this.ClearItemList();
                if (files.Count > 0)
                {
                    // NEW: display total number of images
                    txtTotal.Text = files.Count.ToString();
                         
                    progressBar.Maximum = files.Count;
                    progressBar.Value = 0;

                    // Enable this button to stop detection
                    startStopButtonToStopMode();

                    ProcessAllFiles(files, this.OutputDirectory);
                }
                else {
                    MessageBox.Show("INFO: No images to process in input directory");
                }
            }
            finally {
                selectInputDirBtn.Enabled = true;
                startStopDectectionBtn.Enabled = false;
                firstSpottedCat = true;
                saveResultsBtn.Enabled = true;
                btnClearResults.Enabled = true;
                btnDetectorSettings.Enabled = true;
            }
        }

        private double getConfidenceLevel() 
        {
            double confLev = (double)1 -(double) currentDetectorParams.Sensitivity / 100;
            return Math.Max(Math.Min(confLev, 1), 0);
        }

        private void ProcessAllFiles(List<FileInfo> inputFiles, string outputDir)
        {
            #region set detection parameters (these were defined as part of the 'training' phase of the project, where Monte Carlo simulations defined the optimal settings
            //detectorParams.CheetahModels.Add("cheetah2.png");
            //detectorParams.CheetahModels.Add("cheetah1.png");
            //detectorParams.CheetahModels.Add(@"C:\SpottedCat\Images\ALL\SILUETA.jpg");
            //detectorParams.CheetahModels.Add(@"C:\SpottedCat\Images\ALL\SILUETA2.jpg");
            currentDetectorParams.PositiveModels.Clear();
            for (int k = 0; k <= 1; k++)
            {
                currentDetectorParams.PositiveModels.Add(Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "feature" + k.ToString() + ".png"));
            }




            #endregion

            #region More from training phase : solution verifier encapsulate the knowledge from a known set of input images, used for training
            solutionVerifier.ROC.Reset();
            #endregion

            // Create our tailor made detector
            SpottedCatDetector detector = new SpottedCatDetector(currentDetectorParams);
            SpottedCatChallengeInput detectorInput = new SpottedCatChallengeInput();
            detectorInput.ConfidenceLevelZeroToOne = this.getConfidenceLevel(); // 0.8 is the default, but set lower so to allow for more detections
            
            // Set event handlers
            detector.OnProgressStep += new SpottedCatDetector.OnProgressStepEventHandler(detector_OnProgressStep);
            detector.OnFinishProcessImage += new SpottedCatDetector.OnFinishProcessImageEventHandler(detector_OnFinishProcessImage);
            detector.OnErrorProcessImage += new SpottedCatDetector.OnErrorProcessImageEventHandler(detector_OnErrorProcessImage);
           
            // Create a list of strings ...
            //inputFiles.ForEach(delegate(FileInfo info) { detectorInput.InputFilePaths.Add(info.FullName); });
            detectorInput.InputFiles = inputFiles;

            try
            {
                // Go Detector go !!!
                detectionResults = detector.DetectSpottedCats(detectorInput);
                serializeDetectionResultsToXml();
                serializeDetectionResultsToCsv();
                serializeErrorMessagesToTxt();

                if (DetectorShouldContinue)
                {
                    progressBar.Value = progressBar.Maximum; // just in case they were errors
                    if (!isDebugMode) MessageIndication("Process complete !!");
                    startStopButtonToStartMode();
                }
                else 
                {
                    if (!isDebugMode) MessageIndication("Detection stopped by the user.");
                    startStopButtonToStartMode();
                }
            }
            catch (Exception e)
            {
                MessageIndication("Exception while processing input files, message: " + e.Message);
            }
        }

        void detector_OnErrorProcessImage(EventArgs e, string path, string errorMessage)
        {
            bool detectorShouldContinue = this.DetectorShouldContinue;
            MessageIndication("Exception while processing input file: " + path + ", message: " + errorMessage);
            detector_OnProgressStep(e, ref detectorShouldContinue);
            Application.DoEvents();
        }

        private void detector_OnProgressStep(EventArgs e, ref bool detectorShouldContinue)
        {
            detectorShouldContinue = this.DetectorShouldContinue;
            countScannedImgs++;
            txtScanned.Text = countScannedImgs.ToString();
            progressBar.PerformStep();
            Application.DoEvents();
        }


        private void ClearItemList()
        {
            listViewImages.Items.Clear();

            lvgIs = listViewImages.Groups["lvGroupSpottedCats"];
            lvgIsNot = listViewImages.Groups["lvGroupOthers"];
            Application.DoEvents();
        }




        private void AddItemToList(string filepath, double probability, bool isSpottedCat) 
        { 
            ListViewGroup lvg = isSpottedCat ? lvgIs : lvgIsNot;
            ListViewItem lvi = new ListViewItem(Path.GetFileName(filepath), lvg);
            lvi.Tag = filepath;
            string isSpottedCatStr = isSpottedCat ? "Y" : "N";
            System.Windows.Forms.ListViewItem.ListViewSubItem lvsi = lvi.SubItems.Add(probability.ToString("0.##"));
            lvsi.Tag = probability;
            lvsi = lvi.SubItems.Add(isSpottedCatStr);
            lvsi.Tag = isSpottedCat;
            listViewImages.BeginUpdate();
            try
            {
                listViewImages.Items.Add(lvi);
                //listViewImages.Sort();
            }
            finally
            {
                listViewImages.EndUpdate();
            }
            if (isSpottedCat && firstSpottedCat) { firstSpottedCat = false; imageBox1.BackgroundImage = null; imageBox1.Invalidate(); lvi.Selected = true; listViewImages.Select(); Application.DoEvents(); }
        }

        private string getOutputPositiveDir()
        {
            return Path.Combine(outputDirectory + PATH_POSITIVE_IMAGES);
        }

        private string getOutputNegativeDir()
        {
            return Path.Combine(outputDirectory + PATH_NEGATIVE_IMAGES);
        }

        private string getOutputDebugDir()
        {
            return Path.Combine(outputDirectory + PATH_DEBUG_IMAGES);
        }

        private void saveResultImages(string filepath, bool isSpottedCat, SpottedCatDetector detector)
        {
            bool modeCopyImagesToOutput = true; // FIXME

            try
            {
                if (modeCopyImagesToOutput)
                {
                    string imageName = Path.GetFileName(filepath);
                    if (isSpottedCat)
                    {
                        string pathOutputPos = getOutputPositiveDir();
                        string pathOutputDebug = getOutputDebugDir();
                        if (!Directory.Exists(pathOutputPos)) Directory.CreateDirectory(pathOutputPos);
                        if (!Directory.Exists(pathOutputDebug)) Directory.CreateDirectory(pathOutputDebug);

                        File.Copy(filepath, Path.Combine(pathOutputPos, imageName));

                        if (currentDetectorParams.GenerateDebugImagesPOS)
                        {
                            detector.DiagnoseModeResultImg.Save(Path.Combine(getOutputDebugDir(), imageName));
                        }
                    }
                    else
                    {
                        string pathOutputNeg = getOutputNegativeDir();
                        string pathOutputDebug = getOutputDebugDir();
                        if (!Directory.Exists(pathOutputNeg)) Directory.CreateDirectory(pathOutputNeg);
                        if (!Directory.Exists(pathOutputDebug)) Directory.CreateDirectory(pathOutputDebug);
                        File.Copy(filepath, Path.Combine(pathOutputNeg, imageName));

                        if (currentDetectorParams.GenerateDebugImagesNEG)
                        {
                            detector.DiagnoseModeResultImg.Save(Path.Combine(getOutputDebugDir(), imageName));
                        }
                    }                
                }
            } 
            catch(Exception excp) {}
        }

        private void detector_OnFinishProcessImage(EventArgs e, SpottedCatDetector detector, string filepath, double probability, bool isSpottedCat)
        {
            solutionVerifier.IsSolutionOK(Path.GetFileName(filepath).ToUpper(), isSpottedCat);
            AddItemToList(filepath, probability, isSpottedCat);

            //detector.DiagnoseModeResultImg.Save(Path.Combine(outputDirectory, Path.GetFileName(filepath))); 
           
            if (isSpottedCat)
            {
                Positives++;
            }
            else
            {
                Negatives++;
            }

            saveResultImages(filepath, isSpottedCat, detector);

            /*string isSpottedCatStr = isSpottedCat ? "Y" : "N";
            string text = filepath + ", " + isSpottedCatStr + ", P=" + probability.ToString("0.##") + ", ACC="+ solutionVerifier.ROC.ACC.ToString("0.##") + "\r\n";
            txtResults.AppendText(text);*/
            if (isDebugMode)
            {
                lblMessages.Text = "Accuracy: " + solutionVerifier.ROC.ACC.ToString() +
                    " -- TPR = " + solutionVerifier.ROC.TPR.ToString("0.##") +
                    " -- FNR = " + solutionVerifier.ROC.FNR.ToString("0.##") +
                    " -- FPR : " + solutionVerifier.ROC.FPR.ToString("0.##") +
                    " -- TNR = " + solutionVerifier.ROC.TNR.ToString("0.##");
            }
            else
            {
                lblMessages.Text = "Processed " + filepath;
            }
        }
        
        /// <summary>
        /// Only used in the 'training phase', no implications in the real scenario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            DirectoryInfo di = new DirectoryInfo(@"C:\SpottedCat\Images\setSplit\testSetCheetahs");
            var filters = new[] { ".bmp", ".jpg", "*.png", "*.gif", "*.tiff" }; // add more
            List<FileInfo> files = di.GetFiles("*.*", SearchOption.TopDirectoryOnly).Where(file => filters.Contains(file.Extension, StringComparer.OrdinalIgnoreCase)).ToList();
            List<string> SpottedCats = new List<string>();
            foreach (FileInfo fi in files)
            {
                SpottedCats.Add(Path.GetFileName(fi.Name).ToUpper());

            }

            string path = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "SpottedCatList.txt");
            File.WriteAllLines(path, SpottedCats.ToArray<string>());
        }


        private void listViewImages_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string filepath = (string) listViewImages.SelectedItems[0].Tag;
                if (firstImageSelected)
                {
                    firstImageSelected = false;
                    imageBox1.BackgroundImage = null;
                    imageBox1.Invalidate();
                    Application.DoEvents();
                }
                imageBox1.Load(filepath);
            } catch(Exception) 
            {
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void lblNEG_Click(object sender, EventArgs e)
        {

        }


        private void toggleDetectedForListViewItem(ListViewItem lvi) {
            if (lvi != null) {
                bool isSpottedCat = (bool) lvi.SubItems[2].Tag;

                string pathOutputPos = getOutputPositiveDir();
                string pathOutputNeg = getOutputNegativeDir();
                string sourcePath = "";
                string targetPath = "";
                if (isSpottedCat)
                {
                    lvi.SubItems[1].Text = "0.0";
                    lvi.SubItems[1].Tag = (double) 0.0;
                    lvi.SubItems[2].Text = "N(R)";
                    lvi.SubItems[2].Tag = false;
                    Positives--;
                    Negatives++;
                    try
                    {
                        sourcePath = Path.Combine(pathOutputPos, lvi.SubItems[0].Text);
                        targetPath = Path.Combine(pathOutputNeg, lvi.SubItems[0].Text);
                        if (!Directory.Exists(pathOutputNeg)) Directory.CreateDirectory(pathOutputNeg);
                        File.Move(sourcePath, targetPath);
                    }
                    catch (Exception ee) { MessageIndication("Error while trying to move image: " + sourcePath + " to " + targetPath);}
                }
                else {
                    lvi.SubItems[1].Text = "1.0";
                    lvi.SubItems[1].Tag = (double) 1.0;
                    lvi.SubItems[2].Text = "Y(R)";
                    lvi.SubItems[2].Tag = true;
                    Negatives--;
                    Positives++;
                    // copy file from input directory to Positive output dir 
                    try
                    {
                        sourcePath = Path.Combine(pathOutputNeg, lvi.SubItems[0].Text);
                        targetPath = Path.Combine(pathOutputPos, lvi.SubItems[0].Text);
                        if (!Directory.Exists(pathOutputPos)) Directory.CreateDirectory(pathOutputPos);
                        File.Move(sourcePath, targetPath);
                    }
                    catch (Exception ee) { MessageIndication("Error while trying to move image: " + sourcePath + " to " + targetPath);}
                }
            }
        }

        private void listViewImages_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == ' ') {
                if(listViewImages.SelectedItems != null) {
                    ListViewItem lvi = listViewImages.SelectedItems[0];
                    toggleDetectedForListViewItem(lvi);
                }
            }
        }

        private void serializeDetectionResultsToXml() 
        { 
            string resultsFilePath = Path.Combine(this.OutputDirectory, "DetectionResults.xml");
            try
            {
                if (detectionResults != null)
                {
                    // Serialize SpottedCatChallengeResults to XML
                    detectionResults.Sort();
                    XmlSerializer writer = new XmlSerializer(typeof(SpottedCatChallengeResults));
                    StreamWriter resultsFile = new StreamWriter(resultsFilePath);
                    try
                    {
                        writer.Serialize(resultsFile, detectionResults);
                    }
                    finally
                    {
                        resultsFile.Close();
                    }
                }
            }
            catch (Exception ee)
            {
                MessageIndication("Error while trying to save XML results file: " + resultsFilePath + " - msg: " + ee.Message);
            }
        }

        private void serializeErrorMessagesToTxt()
        {
            string errorsFilePath = Path.Combine(this.OutputDirectory, "ErrorList.txt");
            try
            {
                if (errorList.Count > 0) 
                { 
                    // Serialize results to plain txt
                    StreamWriter errorsFile = new StreamWriter(errorsFilePath);
                    try
                    {
                        string line;
                        foreach (string error in errorList)
                        {
                            errorsFile.WriteLine(error);
                        }
                    }
                    finally
                    {
                        errorsFile.Close();
                    }
                }
            }
            catch (Exception ee)
            {
                MessageIndication("Error while trying to save CSV results file: " + errorsFilePath + " - msg: " + ee.Message);
            }
        }

        private void serializeDetectionResultsToCsv()
        {
            string resultsFilePath = Path.Combine(this.OutputDirectory, "DetectionResults.csv");
            try
            {   
                // Serialize results to plain txt
                StreamWriter resultsFile = new StreamWriter(resultsFilePath);
                try
                {
                    string line;
                    foreach (SpottedCatChallengeResult result in detectionResults.Results)
                    {
                        line = Path.GetFileName(result.ObservedImage) + "," + result.ImageIsSpottedCatConfidenceLevel.ToString();
                        resultsFile.WriteLine(line);
                    }
                }
                finally
                {
                    resultsFile.Close();
                }
            }
            catch (Exception ee)
            {
                MessageIndication("Error while trying to save CSV results file: " + resultsFilePath + " - msg: " + ee.Message);
            }
        }

        private void serializeCorrectedResultsToCsv()
        {
            string resultsFilePath = Path.Combine(this.OutputDirectory, "CorrectedResults.csv");
            try
            {   
                // Serialize results to plain txt
                StreamWriter resultsFile = new StreamWriter(resultsFilePath);
                try
                {
                    string line;
                    foreach (ListViewItem lvi in listViewImages.Items)
                    {
                        line = lvi.Text + "," + ((double) lvi.SubItems[1].Tag).ToString();
                        resultsFile.WriteLine(line);
                    }
                }
                finally
                {
                    resultsFile.Close();
                }
            }
            catch (Exception ee)
            {
                MessageIndication("Error while trying to save CorrectedResults (CSV) file: " + resultsFilePath + " - msg: " + ee.Message);
            }
        }

        private void saveResultsBtn_Click(object sender, EventArgs e)
        {
            serializeDetectionResultsToXml();
            serializeDetectionResultsToCsv();
            serializeCorrectedResultsToCsv();
            MessageIndication("Result logs were saved successfully to output directory: " + this.outputDirectory);
        }

        private void btnZoom_Click(object sender, EventArgs e)
        {
        }

        private void FormTesting_Load(object sender, EventArgs e)
        {
            // NEW : place instruction labels correctly

            labelInst1.Left = 884;
            labelInst1.Top = 22;

            labelInst2.Left = 884;
            labelInst2.Top = 58;

            labelInst3.Left = 884;
            labelInst3.Top = 95;
        }

        private void dirNameTxtBox_TextChanged(object sender, EventArgs e)
        {
            // NEW : Validate if directory exists
            try 
            {
                if (!Directory.Exists(dirNameTxtBox.Text))
                {
                    dirNameTxtBox.BackColor = alertColorInputFolder;
                    startStopDectectionBtn.Enabled = false;
                    changeOutputDirBtn.Enabled = false;
                }
                else
                {
                    dirNameTxtBox.BackColor = SystemColors.Window;
                    this.selectedDirectory = dirNameTxtBox.Text;
                    SetOutputDirectory(this.selectedDirectory);
                }
            }
            catch(Exception excp) 
            {
            }
        }

        private void lblTotal_Click(object sender, EventArgs e)
        {

        }

        private void groupBoxResults_Enter(object sender, EventArgs e)
        {

        }

        private void btnDetectorSettings_Click(object sender, EventArgs e)
        {
            // Create form where user can set detector settings
            FormSettings frmSettings = new FormSettings();

            frmSettings.DetectorParametersToUI(this.currentDetectorParams);
            if (frmSettings.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                frmSettings.UItoDetectionParameters(ref this.currentDetectorParams);
                dirNameTxtBox_TextChanged(null, null);
            }
        }

        private void btnClearResults_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure to clear results ?", "Confirmation",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
            {
                txtTotal.Text = "0";
                txtScanned.Text = "0";
                this.Positives = 0;
                this.Negatives = 0;
                progressBar.Value = progressBar.Minimum;
                dirNameTxtBox.Text = "";
                outputDirTxtBox.Text = "";
                listViewImages.Items.Clear();
                saveResultsBtn.Enabled = false;
                btnClearResults.Enabled = false;

            }
        }

        private void dirNameTxtBox_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.Copy;
            else
                e.Effect = DragDropEffects.None;
        }

        private void dirNameTxtBox_DragDrop(object sender, DragEventArgs e)
        {
            try
            {
                var files = (string[])e.Data.GetData(DataFormats.FileDrop);
                if ((files != null) && (files.Length > 0))
                {
                    string dir = files[0];
                    if (File.Exists(dir)) dir = Path.GetDirectoryName(dir);
                    dirNameTxtBox.Text = dir;
                }
            } catch(Exception)
            {
            }
        }





    }
}
