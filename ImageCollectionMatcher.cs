﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emgu.CV.Util;
using Emgu.CV;
using Emgu.CV.Flann;



namespace SpottedCatChallenge
{

    public class IndecesMapping
    {
        public int IndexStart { get; set; }
        public int IndexEnd { get; set; }
        public int Similarity { get; set; }
        public string fileName { get; set; }
        public double NormalizedSimilarity 
        {
            get { return (double)Similarity / (double)(IndexEnd - IndexStart + 1); }
        }

        public IndecesMapping()
        {
        }
    }

    class IndecesMappingComparer : IComparer<IndecesMapping>
    {
        public int Compare(IndecesMapping x, IndecesMapping y)
        {
            double normSimX = x.NormalizedSimilarity;
            double normSimY = y.NormalizedSimilarity;

            return normSimY.CompareTo(normSimX);
        }
    }


    public class ImageKeyPointsAndDescriptors {
        private string filename;
        private VectorOfKeyPoint keypoints;
        private Matrix<float> descriptors;

        public string Filename
        {
            get { return filename; }
            set { filename = value; }
        }

        public VectorOfKeyPoint Keypoints
        {
            get { return keypoints; }
            set { keypoints = value; }
        }

        public Matrix<float> Descriptors
        {
            get { return descriptors; }
            set { descriptors = value; }
        }

        public ImageKeyPointsAndDescriptors(string filename, VectorOfKeyPoint keypoints, Matrix<float> descriptors) 
        {
            this.filename = filename;
            this.keypoints = keypoints;
            this.descriptors = descriptors;
        }
    }

    public class ImageCollectionMatcher
    {
        private List<ImageKeyPointsAndDescriptors> modelImages;
        private List<Matrix<float>> descs;
        private Matrix<int> indices; 

        public ImageCollectionMatcher()
        {
            modelImages = new List<ImageKeyPointsAndDescriptors>();
            descs = new List<Matrix<float>>();
        }

        public void AddModelImage(string filename, VectorOfKeyPoint keypoints, Matrix<float> descriptors) 
        {
            ImageKeyPointsAndDescriptors imgKeyPoints = new ImageKeyPointsAndDescriptors(filename, keypoints, descriptors);
            modelImages.Add(imgKeyPoints);
        }

        public List<IndecesMapping> Match(ImageKeyPointsAndDescriptors observedImageKeyPointsAndDescriptors)
        {
            List<IndecesMapping> imap;

            // Generate an internal list of model descriptors and a list of indices mapping 
            //(this is to know know which descriptors within the concatenated matrix belong to what image)
            IList<Matrix<float>> dbDescsList = GetMultipleDescriptorsAndIndecesMapping(out imap);

            // concatenate all model images descriptors into single Matrix
            Matrix<float> concatDescsModelImages = ConcatDescriptors(dbDescsList);

            // compute descriptors for the ob image
            Matrix<float> obsImageDescriptors = observedImageKeyPointsAndDescriptors.Descriptors;

            // Find matches
            FindMatches(concatDescsModelImages, obsImageDescriptors, ref imap);

            // Sort by normalized similarity
            imap.Sort(new IndecesMappingComparer());

            return imap;
        }


        public IList<Matrix<float>> GetMultipleDescriptorsAndIndecesMapping(out List<IndecesMapping> imap)
        {
            imap = new List<IndecesMapping>();
            IList<Matrix<float>> descs = new List<Matrix<float>>();
            int idx = 0;
            foreach(ImageKeyPointsAndDescriptors imgKPandDesc in modelImages) 
            {
                Matrix<float> desc = imgKPandDesc.Descriptors;
                descs.Add(desc);
                
                imap.Add(new IndecesMapping()
                {
                    fileName = imgKPandDesc.Filename,
                    IndexStart = idx,
                    IndexEnd = idx + desc.Rows - 1
                });

                idx += desc.Rows;
            }

            return descs;
        }

        public Matrix<float> ConcatDescriptors(IList<Matrix<float>> descriptors)
        {
            int cols = descriptors[0].Cols;
            int rows = descriptors.Sum(a => a.Rows);

            float[,] concatedDescs = new float[rows, cols];
            int offset = 0;

            foreach (var descriptor in descriptors)
            {
                // append new descriptors
                Buffer.BlockCopy(descriptor.ManagedArray, 0, concatedDescs, offset, sizeof(float) * descriptor.ManagedArray.Length);
                offset += sizeof(float) * descriptor.ManagedArray.Length;
            }

            return new Matrix<float>(concatedDescs);
        }


        public void FindMatches(Matrix<float> modelDescriptors, Matrix<float> obsImgDescriptors, ref List<IndecesMapping> imap)
        {
            this.indices = new Matrix<int>(obsImgDescriptors.Rows, 2); // matrix that will contain indices of the 2-nearest neighbors found
            Matrix<float> dists = new Matrix<float>(obsImgDescriptors.Rows, 2); // matrix that will contain distances to the 2-nearest neighbors found

            // create FLANN index with 4 kd-trees and perform KNN search over it look for 2 nearest neighbours
            var flannIndex = new Index(modelDescriptors, 4);
            flannIndex.KnnSearch(obsImgDescriptors, indices, dists, 2, 24);

            // Example
            // Image1 = IndexStart=0, IndexEnd=10
            // Image2 = IndexStart=11, IndexEnd=50
            // Image3 = IndexStart=51 IndexEnd=90

            // indices[i,0] = 12
            // FindLastIndex where IndexStart is less than or equal to indices[i,0]
            // in this case, theImgIdx=1  (IndexStart=11, IndexEnd=50)
            List<int> indexes = new List<int>();
            foreach (var img in imap)
            {
                indexes.Add(img.IndexStart);
            }


            for (int i = 0; i < indices.Rows; i++)
            {
                // filter out all inadequate pairs based on distance between pairs (ratio test mentioned in Lowe's paper)
                if (dists.Data[i, 0] < (0.75 * dists.Data[i, 1]))
                {
                    // find image from the model list to which current descriptor range belongs and increment similarity value.                   
                    int theImgIdx = indexes.FindLastIndex(delegate(int indexStart) { return indexStart <= indices[i,0]; });
                    
                    imap[theImgIdx].Similarity++;
                    /*foreach (var img in imap)
                    {
                        if (img.IndexStart <= indices[i,0] && img.IndexEnd >= indices[i,0])
                        {
                            img.Similarity++;
                            break;
                        }
                    }*/
                }
            }
        }
    }
}
